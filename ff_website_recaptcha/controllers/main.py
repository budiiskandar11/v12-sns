# -*- coding: utf-8 -*-

import odoo
from odoo import http
from odoo.http import request
from odoo.tools.translate import _
from odoo.addons.web.controllers.main import Home
from odoo.addons.auth_signup.controllers.main import AuthSignupHome
from odoo.addons.website_form.controllers.main import WebsiteForm


class Home(Home):
    @http.route('/web/login', type='http', auth="none", sitemap=False)
    def web_login(self, redirect=None, **kw):
        request.params['login_success'] = False
        if not request.uid:
            request.uid = odoo.SUPERUSER_ID
        values = request.params.copy()

        if 'g-recaptcha-response' in kw and \
           not request.website.is_captcha_valid(kw['g-recaptcha-response']):
            values['error'] = _("Required: Invalid/Missing reCaptcha.")
            response = request.render('web.login', values)
            response.headers['X-Frame-Options'] = 'DENY'

            if 'login' not in values and request.session.get('auth_login'):
                values['login'] = request.session.get('auth_login')
            return response
        return super(Home, self).web_login(redirect=redirect, **kw)


class AuthSignupHome(AuthSignupHome):
    @http.route('/web/signup', type='http', auth='public', website=True, sitemap=False)
    def web_auth_signup(self, *args, **kw):
        qcontext = self.get_auth_signup_qcontext()
        if 'g-recaptcha-response' in kw and \
           not request.website.is_captcha_valid(kw['g-recaptcha-response']):
            qcontext['error'] = _("Required: Invalid/Missing reCaptcha.")
            return request.render('auth_signup.signup', qcontext)
        return super(AuthSignupHome, self).web_auth_signup(*args, **kw)


class WebsiteForm(WebsiteForm):
    @http.route()
    def website_form(self, model_name, **kwargs):
        if kwargs.get('g-recaptcha-response'):
            if request.website.is_captcha_valid(kwargs['g-recaptcha-response']):
                del kwargs['g-recaptcha-response']
                return super(WebsiteForm, self).website_form(model_name, **kwargs)
            else:
                return super(WebsiteForm, self).website_form(None, **kwargs)
        return super(WebsiteForm, self).website_form(model_name, **kwargs)
