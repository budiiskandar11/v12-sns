# -*- coding: utf-8 -*-

{
    'name': 'Website reCAPTCHA for Login, Signup, Contactus',
    'version': '1.0',
    'category': 'Website',
    'sequence': 1,
    'summary': 'Google reCaptcha',
    'description': """reCaptcha for Login, Signup, Contactus""",
    'depends': ['auth_signup', 'website_crm'],
    'author': 'Forfens Tech',
    'images': ['static/description/main_screenshot.png'],
    'data': [
        'views/recaptcha_assets.xml',
        'views/recaptcha_views.xml',
        'views/recaptcha_templates.xml',
    ],
    'license': 'OPL-1',
    'installable': True,
    'application': True,
    'price': 49,
    'currency': 'EUR',
    'live_test_url': '',
}
