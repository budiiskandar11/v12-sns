# -*- coding: utf-8 -*-

import logging
import requests

from odoo import api, fields, models

_logger = logging.getLogger(__name__)


class ResConfigSettings(models.TransientModel):
    _inherit = "res.config.settings"

    def _default_website(self):
        return self.env['website'].search([], limit=1)

    website_id = fields.Many2one(
        'website', string="website", default=_default_website, required=True)
    recaptcha_site_key = fields.Char(
        string='Site Key', related='website_id.recaptcha_site_key', readonly=False)
    recaptcha_private_key = fields.Char(
        string='Private Key', related='website_id.recaptcha_private_key', readonly=False)
    enable_recaptcha_login = fields.Boolean(
        string='Enable reCAPTCHA on Login', related='website_id.enable_recaptcha_login', readonly=False)
    enable_recaptcha_signup = fields.Boolean(
        string='Enable reCAPTCHA on Signup', related='website_id.enable_recaptcha_signup',
        readonly=False)
    enable_recaptcha_contactus = fields.Boolean(
        string='Enable reCAPTCHA on Contactus', related='website_id.enable_recaptcha_contactus',
        readonly=False)

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        res.update(
            recaptcha_site_key=self.env['ir.config_parameter'].sudo().get_param(
                'recaptcha_site_key'),
            recaptcha_private_key=self.env['ir.config_parameter'].sudo().get_param(
                'recaptcha_private_key'),
            enable_recaptcha_login=self.env['ir.config_parameter'].sudo().get_param(
                'enable_recaptcha_login'),
            enable_recaptcha_signup=self.env['ir.config_parameter'].sudo().get_param(
                'enable_recaptcha_signup'),
            enable_recaptcha_contactus=self.env['ir.config_parameter'].sudo().get_param(
                'enable_recaptcha_contactus'),
        )
        return res

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        params = self.env['ir.config_parameter'].sudo()
        recaptcha_site_key = self.recaptcha_site_key
        recaptcha_private_key = self.recaptcha_private_key
        enable_recaptcha_login = self.enable_recaptcha_login
        enable_recaptcha_signup = self.enable_recaptcha_signup
        enable_recaptcha_contactus = self.enable_recaptcha_contactus
        params.set_param('recaptcha_site_key', recaptcha_site_key)
        params.set_param('recaptcha_private_key', recaptcha_private_key)
        params.set_param('enable_recaptcha_login', enable_recaptcha_login)
        params.set_param('enable_recaptcha_signup', enable_recaptcha_signup)
        params.set_param('enable_recaptcha_contactus', enable_recaptcha_contactus)


class Website(models.Model):
    _inherit = "website"

    recaptcha_site_key = fields.Char('reCAPTCHA site Key')
    recaptcha_private_key = fields.Char('reCAPTCHA Private Key')
    enable_recaptcha_login = fields.Boolean('Enable reCAPTCHA on Login')
    enable_recaptcha_signup = fields.Boolean('Enable reCAPTCHA on Signup')
    enable_recaptcha_contactus = fields.Boolean('Enable reCAPTCHA on Contactus')

    @api.multi
    def is_captcha_valid(self, cresponse):
        params = {'secret': self.recaptcha_private_key, 'response': cresponse}
        try:
            response = requests.get(
                'https://www.google.com/recaptcha/api/siteverify', params=params)
        except Exception:
            _logger.warning("Could not connect to Google reCAPTCHA server.")
        res = response.json()
        return True if res.get('success') else False
