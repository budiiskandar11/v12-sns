odoo.define('social_sharing.wk_share', function (require) {

    require('web.dom_ready');
    var webSale = require('website_sale.website_sale');

    $("#shareContainer").mouseenter(function() {
        $("#layerDiv").slideDown("fast");
    });
    $("#shareContainer").mouseleave(function() {
        $("#layerDiv").slideUp("fast");
    });

    var pageUrl = window.location.href;

    $('#copyLink').on("click", function(event){
        var elArea = document.createElement("textarea");
        elArea.value = pageUrl;
        document.body.appendChild(elArea);
        elArea.select();
        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
            console.log('Copying text command was ' + msg);
        } catch (err) {
            console.log('Oops, unable to copy');
        }
        document.body.removeChild(elArea);
    });

    var shareText = "Hey, I like this product and I'd like to share it with you. "
    $('#twShare').on("click", function(event){
        var popupUrl = _.str.sprintf( 'https://twitter.com/intent/tweet?tw_p=tweetbutton&text=%s %s', shareText, pageUrl);
        wkSharWindow(popupUrl);

    });

    $('#fbShare').on("click", function(event){
        var popupUrl = _.str.sprintf('https://www.facebook.com/sharer/sharer.php?u=%s', encodeURIComponent(pageUrl));
        wkSharWindow(popupUrl);

    });

    $('#gpShare').on("click", function(event){
        var popupUrl = _.str.sprintf( 'https://plus.google.com/share?url=%s',encodeURIComponent(pageUrl));
        wkSharWindow(popupUrl);

    });

    $('#linkedInShare').on("click", function(event){
        var popupUrl = _.str.sprintf( 'http://www.linkedin.com/shareArticle?mini=true&url=%s&title=%s',encodeURIComponent(pageUrl), shareText);
        wkSharWindow(popupUrl);

    });


    $('#pinShare').on("click", function(event){
        var e = document.createElement('script');
        e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);
        document.body.appendChild(e);

    });

    $('#tmblrShare').on("click", function(event){
        var popupUrl = _.str.sprintf( 'http://tumblr.com/widgets/share/tool?canonicalUrl=%s',encodeURIComponent(pageUrl));
        wkSharWindow(popupUrl);

    });

    $('#redditShare').on("click", function(event){
        var popupUrl = _.str.sprintf( 'http://www.reddit.com/submit?url=%s&title=%s',encodeURIComponent(pageUrl), shareText);
        wkSharWindow(popupUrl);

    });

    $('#bufferShare').on("click", function(event){
        var popupUrl = _.str.sprintf( 'https://bufferapp.com/add?url=%s',encodeURIComponent(pageUrl));
        wkSharWindow(popupUrl);

    });

    $('#stumbleUponShare').on("click", function(event){
        var popupUrl = _.str.sprintf( 'http://www.stumbleupon.com/submit?url=%s',encodeURIComponent(pageUrl));
        wkSharWindow(popupUrl);

    });

    $('#diggShare').on("click", function(event){
        var popupUrl = _.str.sprintf( 'http://www.digg.com/submit?url=%s',encodeURIComponent(pageUrl));
        wkSharWindow(popupUrl);

    });

    $('#diigoShare').on("click", function(event){
        var popupUrl = _.str.sprintf( 'https://www.diigo.com/post?url=%s',encodeURIComponent(pageUrl));
        wkSharWindow(popupUrl);

    });

    $('#plurkShare').on("click", function(event){
        var popupUrl = _.str.sprintf( 'https://www.plurk.com/m?content=%s&qualifier=shares',encodeURIComponent(pageUrl));
        wkSharWindow(popupUrl);

    });

    $('#flipboardShare').on("click", function(event){
        var popupUrl = _.str.sprintf('https://share.flipboard.com/bookmarklet/popout?v=%s&url=%s&ext=addthis&utm_medium=web&utm_campaign=widgets&utm_source=addthis',shareText, encodeURIComponent(pageUrl));
        wkSharWindow(popupUrl);

    });

    $('#hootsuiteShare').on("click", function(event){
        var popupUrl = _.str.sprintf('https://hootsuite.com/hootlet/social-share?source=hootlet&url=%s&title=%s', encodeURIComponent(pageUrl), shareText);
        wkSharWindow(popupUrl);

    });

    $('#bizsugarShare').on("click", function(event){
        var popupUrl = _.str.sprintf( 'http://www.bizsugar.com/bizsugarthis.php?url=%s',encodeURIComponent(pageUrl));
        wkSharWindow(popupUrl);

    });

    $('#houzzShare').on("click", function(event){
        var prodImage = document.images[1].src;
        var popupUrl = _.str.sprintf( 'https://www.houzz.com/imageClipperUpload?imageUrl=%s&title=%s&link=%s',prodImage, shareText, encodeURIComponent(pageUrl));
        wkSharWindow(popupUrl);

    });

    $('#yummShare').on("click", function(event){
        var popupUrl = _.str.sprintf( 'http://www.yummly.com/urb/verify?url=%s&title=%s',encodeURIComponent(pageUrl));
        wkSharWindow(popupUrl);

    });
    

    function wkSharWindow(popupUrl) {
        window.open(
            popupUrl,
            'Share Dialog',
            'width=600,height=400'
        );
    };


});
