# -*- coding: utf-8 -*-
##########################################################################
#
#	Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE file for full copyright and licensing details.
#   "License URL : <https://store.webkul.com/license.html/>"
#
##########################################################################

from odoo import http, tools, _
from odoo.http import request
from odoo.addons.website_sale.controllers.main import WebsiteSale

class WebsiteSale(WebsiteSale):


	@http.route('/share/wapp/product/<int:product_id>', type='http', auth="public", website=True)
	def share_on_wapp(self, product_id=0):
		pagUrl = request.httprequest.url
		shareUrl = "whatsapp://send?text=" + pagUrl
		return request.redirect(shareUrl)
