# -*- coding: utf-8 -*-
##########################################################################
#
#   Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE file for full copyright and licensing details.
#   License URL : <https://store.webkul.com/license.html/>
#
##########################################################################

from odoo import api, fields, models, _
from odoo.http import request
try:
    from user_agents import parse
except Exception as e:
    pass


class ProductTemplate(models.Model):
    _inherit = 'product.template'


    @api.multi
    def share_media(self):
        agentString = request.httprequest.headers.get('User-Agent')
        res = self.env['wk.social.media'].get_shared_media()
        alwsShow = ['fb', 'wapp', 'tw', 'gplus', 'linkedIn', 'enableSharing']
        showShareIcon = list(set(res) - set(alwsShow))
        if showShareIcon:
            res.append('showShareIcon')
        try:
            userAgent = parse(agentString)
            runningOnPc = userAgent.is_pc
            if not runningOnPc:
                res.append('mobile')
        except Exception as e:
            pass
        return res
