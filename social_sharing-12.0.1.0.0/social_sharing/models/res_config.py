# -*- coding: utf-8 -*-
##########################################################################
#
#	Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE file for full copyright and licensing details.
#   "License URL : <https://store.webkul.com/license.html/>"
#
##########################################################################

from odoo import api, models, fields


class WebkulWebsiteAddons(models.TransientModel):
    _inherit = 'webkul.website.addons'

    module_social_sharing = fields.Boolean(string="Website: Social Sharing")


class WkAllowSharing(models.TransientModel):
    _name = 'wk.allow.sharing'
    _inherit = 'res.config.settings'

    enable_sharing = fields.Boolean(string='Enable Website Products Sharing Feature')
    allow_social = fields.Many2many('wk.social.media', 'allow_id', 'media_id',
        'allow_media_share_rel', string='Social Media Platforms')

    @api.multi
    def set_values(self):
        super(WkAllowSharing, self).set_values()
        irDefaultSudo = self.env['ir.default'].sudo()
        allowSocial = self.allow_social and self.allow_social.ids
        enableSharing = self.enable_sharing
        resModel = 'wk.allow.sharing'
        irDefaultSudo.set(resModel, 'allow_social', allowSocial)
        irDefaultSudo.set(resModel, 'enable_sharing', enableSharing)
        return True

    @api.model
    def get_values(self):
        res = super(WkAllowSharing, self).get_values()
        resModel = 'wk.allow.sharing'
        irDefaultSudo = self.env['ir.default'].sudo()
        allowSocial = irDefaultSudo.get(resModel, 'allow_social')
        enableSharing = irDefaultSudo.get(resModel, 'enable_sharing')
        res.update({
            'allow_social':allowSocial,
            'enable_sharing':enableSharing,
        })
        return res
