# -*- coding: utf-8 -*-
##########################################################################
#
#	Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE file for full copyright and licensing details.
#   "License URL : <https://store.webkul.com/license.html/>"
#
##########################################################################

from odoo import api, models, fields
from odoo.http import request

media = [
    ('fb', 'Facebook'),
    ('wapp', 'Whatsapp'),
    ('tw', 'Twitter'),
    ('gplus', 'Google Plus'),
    ('linkedIn', 'linkedIn'),
    ('pint', 'Pinterest'),
    ('tmblr', 'Tumblr'),
    ('reddit', 'reddit'),
    ('buffer', 'Buffer '),
    ('stumble', 'StumbleUpon'),
    ('digg', 'Digg'),
    ('diigo', 'Diigo'),
    ('plurk', 'Plurk'),
    ('flipboard', 'Flipboard'),
    ('hootsuite', 'Hootsuite'),
    ('bizsugar', 'BizSugar'),
    ('houzz', 'Houzz'),
]


class WkSocialMedia(models.Model):
    _name = 'wk.social.media'
    _description = 'Webkul Social Media'

    name = fields.Selection(selection=media, string="Media")
    color = fields.Integer(string='Color Index')

    @api.model
    def get_shared_media(self):
        allowMedia = []
        irDefaultSudo = self.env['ir.default'].sudo()
        sharedMedia = irDefaultSudo.get(
            'wk.allow.sharing', 'allow_social')
        enableSharing = irDefaultSudo.get(
            'wk.allow.sharing', 'enable_sharing')
        if enableSharing:
            allowMedia.append('enableSharing')
        for mediaObj in self.browse(sharedMedia):
            allowMedia.append(mediaObj.name)
        return allowMedia

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            showName = dict(
                self.fields_get(
                    allfields=['name']
                    )['name']['selection']
                )[record.name]
            res.append((record.id, showName))
        return res

    @api.model
    def _wk_demo_share(self):
        configModel = self.env['wk.allow.sharing']
        shareId1 = self.env.ref('social_sharing.wk_social_media_1').id
        shareId2 = self.env.ref('social_sharing.wk_social_media_2').id
        shareId3 = self.env.ref('social_sharing.wk_social_media_3').id
        shareId4 = self.env.ref('social_sharing.wk_social_media_4').id
        shareIds = [shareId1, shareId2, shareId3, shareId4]
        shreObj = configModel.create({
            'allow_social':[(6, 0, shareIds)],
            'enable_sharing' : True
            })
        res = shreObj.execute()
        return True