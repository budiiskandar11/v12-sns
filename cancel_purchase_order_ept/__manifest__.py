{
    
    # App information
    'name': 'Cancel Purchase Order In Odoo',
    'version': '12.0',
    'category': 'purchase',
    'summary' : 'Allow to Cancel such Purchase Orders whose incoming shipment is processed / invoice is generated.',
    'license': 'OPL-1',
   
    
     # Author
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',
    'maintainer': 'Emipro Technologies Pvt. Ltd.',
    
          
    
    
    # Dependencies
    
    'depends': ['purchase','cancel_stock_picking_ept'],
    
    
    
    # Views
    'data': [
     'view/purchase_order.xml'
    ],
    
    'demo': [
    ],

   # Odoo Store Specific
    'images': ['static/description/Odoo-covor.jpg'],
    'live_test_url' : 'https://www.emiprotechnologies.com/free-trial?app=cancel-purchase-order-ept&version=12&edition=enterprise',
    'price': '40' ,
    'currency': 'EUR',
    'installable': True,
    'auto_install': False,
    'application': True,
}
