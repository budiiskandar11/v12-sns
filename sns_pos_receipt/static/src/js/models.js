odoo.define('sns_pos_receipt.models', function (require) { 
 "use strict";

var models = require('point_of_sale.models'); 
var _super_posmodel = models.PosModel.prototype;

models.PosModel = models.PosModel.extend({
       initialize: function (session, attributes) {

             //push fields street, npwp, city, report logo in model res.company
             for(var i=0;i<this.models.length;i++){
             var model = this.models[i]
             if(model.model === 'res.company'){
            	   model.fields.push('partner_id');
            	   model.fields.push('street');
                   model.fields.push('city');
                   model.fields.push('zip');
                   model.fields.push('street2');
                   model.fields.push('report_company_logo');
                  }
             if(model.model === 'res.partner'){
          	   	   model.fields.push('npwp');
             }
             }
             
         return _super_posmodel.initialize.call(this, session, attributes);
     },
 });

});
