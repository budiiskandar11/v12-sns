""" model res company """
from odoo import models, fields


class ResCompany(models.Model):
    """res company"""
    _inherit = "res.company"

    report_company_logo = fields.Binary()
    fax = fields.Char(related='partner_id.fax', store=True)
