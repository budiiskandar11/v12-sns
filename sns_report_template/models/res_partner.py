""" model res partner """
from odoo import models, fields


class ResPartner(models.Model):
    """res partner"""
    _inherit = "res.partner"

    fax = fields.Char()
