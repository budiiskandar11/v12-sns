{
    "name": " Custom Header and Footer Template Report",
    "summary": "Custom Header and Footer Template Report",
    "version": "12.0.1.0.0",
    "author": "Portcities Ltd",
    "website" : "https://www.portcities.net",
    "license": "AGPL-3",
    "complexity": "normal",
    "description": """

v1.1
----
* Customize default header and footer report template

*Author : Andreas Dian SP

                      """,
    "category": "Reporting",
    "depends": [
        'base',
        'web',
        'efaktur',
    ],
    "data": [
        'reports/report_template.xml',
        'views/report_view.xml',
        'views/res_company_view.xml',
        'views/res_partner_view.xml',
    ],
    "test": [
    ],
    "auto_install": False,
    'installable': True,
    "application": False,
}
