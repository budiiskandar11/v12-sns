Custom report filenames
=======================

This addon do custom for delivery slip.

Configuration
=============

To configure this module, ...

Bug Tracker
===========

Bugs are tracked on `GitLab Issues <https://>`_.
In case of trouble, please check there if your issue has already been reported.
If you spotted it first, help us smashing it by providing a detailed and welcomed feedback
`here <https://>`_.

.. image:: https://odoo-community.org/website/image/ir.attachment/5784_f2813bd/datas
    :alt: Try me on Runbot
    :target: https://runbot....

Credits
=======

Contributors
------------

* Port Cities

Icon
----


Maintainer
----------

.. image:: http://odoo-community.org/logo.png
   :alt: Odoo Community Association
   :target: http://odoo-community.org

This module is maintained by Port Cities.

