# -*- coding: utf-8 -*-
# Copyright (C) 2017 Portcities Indonesia (<http://portcities.net>).
# All Rights Reserved
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).
{
    "name": "SNS Custom Delivery Order Report",
    "summary": "Customization for Delivery Order slip",
    "version": "12.0.1.0.0",
    "author": "Portcities Indonesia",
    "license": "AGPL-3",
    "complexity": "normal",
    "description": """

v1.1
----
* Customize Delivery Order Report Template

*Author : Arimasendy W.

                      """,
    "category": "Reporting",
    "depends": [
        'base',
        'stock',
        'sale_stock',
    ],
    "data": [
        'reports/report_deliveryslip.xml',
    ],
    "test": [
    ],
    "auto_install": False,
    'installable': True,
    "application": False,
}
