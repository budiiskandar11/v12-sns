# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import json

from odoo import http, tools
from odoo.http import request
from odoo.addons.website_sale.controllers.main import WebsiteSale

class WebsiteSales(WebsiteSale):
    """ Inherit Website Sales Controller object """
    @http.route(['/shop/cart'], type='http', auth="public", website=True)
    def cart(self, access_token=None, revive='', **post):
        """ Override function cart on websitesale controller"""
        response = super(WebsiteSales, self).cart(**post)
        order = request.website.sale_get_order()
        if order.carrier_id:
            for line in order.order_line:
                if line.product_id.id == order.carrier_id.product_id.id:
                    line.sudo().unlink()
            order.carrier_id = False
        if order.reward_amount:
            order.amount_total = order.amount_total + order.reward_amount
        return response
