# """
#  This module is developed by Portcities Indonesia
#  Copyright (C) 2017 Portcities Indonesia (<http://portcities.net>).
#  All Rights Reserved
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
# """

{
    "name": "SNS Custom Sale",
    "summary": "Customize Sale",
    "version": "12.0.1.0.0",
    "author": "Portcities Ltd",
    "website" : "https://www.portcities.net",
    "license": "AGPL-3",
    "complexity": "normal",
    "description": """

v1.1
----
* Readonly : Price in SO line

*Author : Ugi.
v.1.2
----
* Fixing Calculation discount promotion for delivery charge
* Author : AK

                      """,
    "category": "Sales",
    "depends": ['website_sale_coupon',
                'website_sale_delivery'],
    "data": ['views/sale_views.xml',
            'views/website_order.xml',
            'report/sale_report_templates.xml'
             ],
    "test": [],
    "auto_install": False,
    'installable': True,
    "application": False,
}
