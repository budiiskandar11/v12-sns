""" model """
from odoo import models, fields


class SaleOrder(models.Model):
    '''inherit model sale.order'''
    _inherit = 'sale.order'

    def _get_reward_values_discount_percentage_per_line(self, program, line):
        '''replace function and add condition for check product is not type service'''
        if line.product_id.type != 'service':
            discount_amount = line.product_uom_qty * line.price_unit * (program.discount_percentage / 100)
            return discount_amount

class SaleOrderLine(models.Model):
    '''inherit model sale.order.line'''
    _inherit = 'sale.order.line'

    bi_image = fields.Binary(related='product_id.image_medium', string='Image', store=True)
