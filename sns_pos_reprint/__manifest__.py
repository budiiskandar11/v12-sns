{
    'name': 'SNS - POS Reprint',
    'version': '11.0.1.0.0',
    'depends': ['point_of_sale',
                'sns_pos_salesman_selection',
                'sns_report_template'],
    'author': 'Port Cities Ltd',
    'summary': 'Reprint POS receipt',
    'description': """
        v.1.0.0\n
        - Add button reprint in POS order to reprint receipt pos order\n
        author : Rofi S.A\n
    """,
    'website': 'https://www.portcities.net',
    'category': 'pos',
    'data': [
        'data/report_paperformat.xml',
        'report/pos_reprint_report_template.xml',
        'views/pos_order_view.xml',
    ],
    "qweb":[
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
