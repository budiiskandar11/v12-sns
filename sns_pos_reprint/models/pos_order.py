# -*- coding: utf-8 -*-
"""
pos.order python
"""

from datetime import datetime
from odoo import models, api, fields, _


class PosOrder(models.Model):
    """
    inherit pos.order
    """
    _inherit = 'pos.order'

    @api.multi
    def reprint_receipt(self):
        return self.env.ref('sns_pos_reprint.action_report_pos_receipt').report_action(self)
