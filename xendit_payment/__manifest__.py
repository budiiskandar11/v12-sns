# -*- coding: utf-8 -*-

{
    'name': 'Xendit Payment Acquirer',
    'category': 'Hidden',
    'summary': 'Xendit Payment Gateway',
    'version': '1.3',
    'description': """

    """,
    'author': 'budiiskandar11@gmail.com',
    'depends': ['payment'],
    'data': [
        'views/xendit.xml',
        'views/payment_acquirer.xml',
        # 'views/res_config_view.xml',
        'data/xendit.xml',
    ],
    'installable': True,
}
