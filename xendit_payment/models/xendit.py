# -*- coding: utf-'8' "-*-"

import base64
import requests
try:
    import simplejson as json
except ImportError:
    import json
import logging
from urllib.parse import urljoin
# import urllib
# try:
#     from urllib.request import urlopen  # pylint: disable=deprecated-module
# except ImportError:
#     from urllib import urlopen  # pylint: disable=deprecated-module
import urllib.parse
import urllib.request
from werkzeug import urls

from odoo.addons.payment.models.payment_acquirer import ValidationError
from odoo.addons.xendit_payment.controllers.main import XenditController
from odoo import api, fields, models, _
from odoo.tools.float_utils import float_compare
from odoo import SUPERUSER_ID

_logger = logging.getLogger(__name__)

class AcquirerXendit(models.Model):
    _inherit = 'payment.acquirer'

    provider = fields.Selection(selection_add=[('xendit', 'Xendit')], default='xendit')

    xendit_keyserver_account = fields.Char('xendit Key Server', help='The Merchant Key Server is used to ensure communications coming from xendit are valid and secured.')        
    xendit_email_account = fields.Char('xendit Email ID', required_if_provider='xendit')
    xendit_seller_account = fields.Char('xendit Merchant ID', help='The Merchant ID is used to ensure communications coming from xendit are valid and secured.')
    xendit_use_ipn = fields.Boolean('Use IPN', help='xendit Instant Payment Notification', default=True)
    # Server 2 server
    xendit_api_enabled = fields.Boolean('Use Rest API', default=False)
    xendit_api_username = fields.Char('Rest API Username')
    xendit_api_password = fields.Char('Rest API Password')
    xendit_api_access_token = fields.Char('Access Token')
    xendit_api_access_token_validity = fields.Datetime('Access Token Validity')

    fees_active = fields.Boolean(default=False)
    fees_dom_fixed = fields.Float(default=0.35)
    fees_dom_var = fields.Float(default=3.4)
    fees_int_fixed = fields.Float(default=0.35)
    fees_int_var = fields.Float(default=3.9)

    def _get_feature_support(self):
        """Get advanced feature support by provider.

        Each provider should add its technical in the corresponding
        key for the following features:
            * fees: support payment fees computations
            * authorize: support authorizing payment (separates
                         authorization and capture)
            * tokenize: support saving payment data in a payment.tokenize
                        object
        """
        res = super(AcquirerXendit, self)._get_feature_support()
        res['fees'].append('xendit')
        return res

    @api.model
    def _get_xendit_urls(self, environment):
        """ xendit URLS """
        print ("================== xendit_get_form_action_url===================")
        if environment == 'prod':
            return {
                'return_url': 'https://api.xendit.co/v2/invoices',
                'xendit_rest_url': 'https://api.xendit.co/v2/invoices',

            }
        else:
            return {
                'return_url': 'https://api.xendit.co/v2/invoices',
                'xendit_rest_url': 'https://api.xendit.co/v2/invoices',
            }


    @api.multi
    def xendit_compute_fees(self, amount, currency_id, country_id):
        """ Compute xendit fees.

            :param float amount: the amount to pay
            :param integer country_id: an ID of a res.country, or None. This is
                                       the customer's country, to be compared to
                                       the acquirer company country.
            :return float fees: computed fees
        """
        if not self.fees_active:
            return 0.0
        country = self.env['res.country'].browse(country_id)
        if country and self.company_id.country_id.id == country.id:
            percentage = self.fees_dom_var
            fixed = self.fees_dom_fixed
        else:
            percentage = self.fees_int_var
            fixed = self.fees_int_fixed
        fees = (percentage / 100.0 * amount + fixed ) / (1 - percentage / 100.0)
        return fees


    @api.multi
    def xendit_form_generate_values(self, values):
        # _logger.info(".............start ...........xendit_form_generate_values...........")
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        
        xendit_values = dict(values)

        server_key = self.xendit_keyserver_account or ""
        if not server_key.endswith(":"):
            server_key = server_key + ":"
        base64string = base64.b64encode(server_key.encode('utf-8',errors = 'strict'))
        headers = {'Content-type': 'application/json',
                   'Accept': 'application/json',
                   'Authorization': "Basic %s" % base64string.decode("utf-8") 
                   }
        req = {
            'server_key' : server_key,
            # 'payment_type': 'vtweb',
            'transaction_details': {'order_id': str(values['reference']),'gross_amount': str(values['amount'])},
            # 'vtweb': {'credit_card_3d_secure': True,},
            'customer_details': {
                                    'first_name': str(values['partner_name']),
                                    # 'last_name': str(values['last_name']),
                                    'email': str(values['partner_email']),
                                    'phone': str(values['partner_phone']),
                                    'billing_address' : {
                                                        'first_name': str(values['billing_partner_first_name']),
                                                        'last_name': str(values['billing_partner_last_name']), 
                                                        'phone': str(values['billing_partner_phone']),
                                                        'address': str(values['billing_partner_address']),
                                                        'city': str(values['billing_partner_city']),
                                                        'postal_code': str(values['billing_partner_zip']),
                                                        },
                                    'shipping_address' : {
                                                         'last_name': str(values['billing_partner_last_name']), 
                                                         'address': str(values['billing_partner_address']),
                                                         'city': str(values['billing_partner_city']),
                                                         'postal_code': str(values['billing_partner_zip']),
                                                        }
                                                         
            }
        }
        
        # 'custom': json.dumps({'return_url': '%s' % xendit_values.pop('return_url')}) if xendit_values.get('return_url') else False,
        # 'xendit_return': '%s' % urlparse.urljoin(base_url, xenditController._return_url),
        # 'handling': '%.2f' % xendit_values.pop('fees', 0.0) if self.fees_active else False,

        # kuncinya ada di sini biar bisa pass ke payment
        xendit_values.update(req)
        

        data = json.dumps(req)
        url = 'https://api.xendit.co/v2/invoices'
        real_amount = values['amount']
        xendit_amount = real_amount
        
        # try to convert amount to IDR
        # since xendit only support IDR as default
        # search for related order to get currency
        sale_ids = self.env['sale.order'].sudo().search([
                ('name', '=', values['reference'])
            ], limit=1)
        if sale_ids:
            currency = sale_ids.currency_id
            if currency.name != 'IDR':
                # check if current IDR is active
                currency_idr = self.env['res.currency'].sudo().search([
                    ('name', '=', 'IDR'), ('active', '=', True)
                    ], limit=1)
                if currency_idr:
                    # if IDR is active, convert amount to IDR
                    xendit_amount = currency.compute(
                        real_amount,
                        currency_idr,
                        True
                        )
        
        print('real amount = %s .........' %real_amount)
        print('xendit_amounts = %s .........' %xendit_amount)
        
        body = {
            'external_id':values['reference'],
            'payer_email':values['partner_email'],
            'description':values['reference'],
            'amount':int(xendit_amount),
            'success_redirect_url': '%s' % urljoin(base_url, '/shop/payment/validate'),
            'failure_redirect_url': '%s' % urljoin(base_url, '/shop/payment/validate'),
        }
        resultreq = requests.post(url=url, headers=headers, data=json.dumps(body))
        result_json = resultreq.json()

        xendit_values.update({'redirect_url' : result_json.get('invoice_url', "")})
        
        return xendit_values
    
    @api.multi
    def xendit_get_form_action_url(self):
        return self._get_xendit_urls(self.environment)['return_url']
    

    def transfer_get_form_action_url(self):
        return '/payment/tes/'


class Txxendit(models.Model):
    _inherit = 'payment.transaction'

    xendit_txn_id = fields.Char('Transaction ID')
    xendit_txn_type = fields.Char('Transaction type')

    # --------------------------------------------------
    # FORM RELATED METHODS
    # --------------------------------------------------

    @api.model
    def _xendit_form_get_tx_from_data(self, xendit_data):
        reference = xendit_data.get('external_id')
#         print ('_xendit_form_get_tx_from_data')
        if not reference:
            error_msg = _('xendit: received data with missing reference (%s)') % (reference)
            _logger.info(error_msg)
            raise ValidationError(error_msg)

        # find tx -> @TDENOTE use txn_id ? --> in there changed to be order_id
        txs = self.env['payment.transaction'].sudo().search([('reference', '=', reference)])
        
        if not txs or len(txs) > 1:
            error_msg = 'xendit: received data for reference %s' % (reference)
            if not txs:
                error_msg += '; no order found'
            else:
                error_msg += '; multiple order found'
            _logger.info(error_msg)
            raise ValidationError(error_msg)
        return txs[0]

    @api.multi
    def _xendit_form_get_invalid_parameters(self, xendit_data):
        _logger.info ("_xendit_form_get_invalid_parameters")
        invalid_parameters = []
#         print ('_xendit_form_get_invalid_parameters')
        # TODO: txn_id: shoudl be false at draft, set afterwards, and verified with txn details --> in there changed to be order_id
        if self.acquirer_reference and xendit_data.get('external_id') != self.acquirer_reference:
            invalid_parameters.append(('order_id', xendit_data.get('external_id'), self.acquirer_reference))

        return invalid_parameters

    @api.multi
    def _xendit_form_validate(self, xendit_data):
#         print ('_xendit_form_validate')
        status = xendit_data.get('status')
        data = {
            'acquirer_reference': xendit_data.get('external_id'),#transaction id
            'xendit_txn_id': xendit_data.get('id'),#transaction id
            'xendit_txn_type': xendit_data.get('payment_method'),#transaction id
        }
        if status in ['PENDING', 'SETTLED']:
            _logger.info('Received notification for xendit payment %s: set as pending' % (self.reference))
            data.update(state='pending', state_message=xendit_data.get('pending_reason', ''))
            return self.write(data)
        elif status in ['PAID']:
            _logger.info('Received notification for xendit payment %s: set as done' % (self.reference))
            data.update(state='done', state_message=xendit_data.get('done', ''))
            return self.write(data)
        else:
            error = 'Received unrecognized status for xendit payment %s: %s, set as error' % (self.reference, status)
            _logger.info(error)
            data.update(state='error', state_message=error)
            return self.write(data)