# -*- coding: utf-8 -*-

try:
    import simplejson as json
except ImportError:
    import json
import logging
import pprint
import urllib
import werkzeug
from odoo.http import request
import yaml

from odoo import http, SUPERUSER_ID

_logger = logging.getLogger(__name__)


class XenditController(http.Controller):
    _return_url = '/payment/xendit/return/'#finish redirect URL
    
    @http.route('/payment/xendit/return/', type='json', auth='public', website=True, csrf=False)
    def xendit_return(self, **kwargs):
        """ xendit callback after payment. """
        
        byte_data = request.httprequest.get_data()
        data = yaml.load(byte_data)
        

        request.env['payment.transaction'].form_feedback(data, 'xendit')
        return werkzeug.utils.redirect('/payment/process')
        # return werkzeug.utils.redirect('/shop/payment/get_status/<int:order_id>')
        
class XenditPaymentController(http.Controller):
    _accept_url = '/payment/xendit/'

    @http.route([
        '/payment/xendit/',
    ], type='http', auth='none', csrf=False)
    def transfer_tes(self, **post):
        _logger.info('Beginning form_feedback with post data %s', pprint.pformat(post))
#         return werkzeug.utils.redirect(post.pop('redirect_url', '/'))
        redirect_url = ''
        if post.get('redirect_url'):
            redirect_url = post.get('redirect_url')
        return werkzeug.utils.redirect(redirect_url)
