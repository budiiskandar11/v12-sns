import time
from odoo import api, fields, models
import odoo.addons.decimal_precision as dp


class midtrans_account(models.Model):
    _inherit = 'res.company'

    midtrans_account = fields.Char('Midtrans Account', size=0)
