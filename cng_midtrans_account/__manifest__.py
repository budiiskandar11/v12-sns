{
    'name'          : 'Add field midtrans_account',
    'category'      : 'companies model/module account',
    'version'       : '1.0',
    'depends'       : ["base","account"],
    'author'        : 'Port Cities - SAR',
    'description'   : '''
	Adding field midtrans account
	''',
    'init_xml'      : [],
    'demo_xml'      : [],
    'installable'   : True,
    'active'        : False,
}