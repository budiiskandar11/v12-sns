# -*- coding: utf-8 -*-
# """
#  This module is developed by Portcities Indonesia
#  Copyright (C) 2017 Portcities Indonesia (<http://portcities.net>).
#  All Rights Reserved
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
# """

{
    "name": "SNS POS Salesman Selection and Set Default Customer",
    "summary": "Add Salesperson in POS Odoo and set default customer",
    "version": "11.0.1.0.0",
    "author": "Portcities Ltd",
    "website" : "https://www.portcities.net",
    "license": "AGPL-3",
    "complexity": "normal",
    "description": """

v1.0
----
* Add Salesperson in POS Odoo
* Author : APR.

v2.0
----
* Set default customer in PoS configuration and set on PoS screen
* Author : A.W.

v2.1
----
* Raise alert when total paid with cash less than total bill
* Author : A.W.

                      """,
    "category": "POS",
    "depends": ['point_of_sale', 'hr'],
    "data": [
        'view/posorder.xml',
        'view/pos_config_view.xml',
        'view/templates.xml'
    ],
    "qweb":[
        'static/src/xml/partner.xml'
    ],
    "test": [],
    "auto_install": False,
    'installable': True,
    "application": False,
}
