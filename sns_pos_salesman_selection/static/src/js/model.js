odoo.define('sns_pos_salesman_selection', function (require) {
	"use strict";

	var models = require('point_of_sale.models')

	var _super_ordermodel = models.Order.prototype;

	models.Order = models.Order.extend({
        initialize: function (attributes, options) {
            this.salesperson = "";
            return _super_ordermodel.initialize.call(this, attributes, options);
        },
        export_as_JSON: function(){
            var json = _super_ordermodel.export_as_JSON.apply(this,arguments);
            json.salesperson = this.salesperson;
            return json;
        },
        init_from_JSON: function(json){
            _super_ordermodel.init_from_JSON.apply(this,arguments);
            this.salesperson = json.salesperson;
        },
    });

	var _modelproto = models.PosModel.prototype;
	models.PosModel = models.PosModel.extend({
		add_new_order: function(){
	    	var res = _modelproto.add_new_order.call(this);
	    	var order = this.get_order();
	    	this.set_default_client(order)
	        return res;
	    },
	    set_default_client: function(order){
	    	if (order) {
	    		var default_client = order.pos.config.default_customer_id
		    	order.set_client(this.db.get_partner_by_id(default_client[0]))
	    	}
	    },
	});

    models.PosModel.prototype.models.push({ //loaded model
        model:  'hr.employee',
        fields: ['id','name'],
        loaded: function(self,employees){ //pass parameters
        self.employees = employees;
        },
    });
});
