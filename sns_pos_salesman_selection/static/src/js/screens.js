odoo.define('sns_pos_salesman_selection.screens', function (require) {
	"use strict";

	var screen = require('point_of_sale.screens')

    screen.PaymentScreenWidget.include({

        order_is_valid: function(force_validation){
            this._super();
            var order = this.pos.get_order();
            order.salesperson = $("#salesperson")[0].value;
            order.salesname = $("#salesname")[0].value;
            var lines = this.pos.get_order().get_paymentlines();
            if (this.pos.get_order().get_total_paid() < this.pos.get_order().get_total_with_tax()) {
            	var cash = false;
                for (var i = 0; i < lines.length; i++) {
                    if (lines[i].cashregister.journal.type === 'cash') {
                    	cash = true;
                    }
                }
                if (cash) {
                	alert('Total paid is less than total bill !');
                	return false;
                }
            }
            return true;
        },
    });
	
	screen.ActionpadWidget.include({
		start: function(){
	        var self = this;
	        this._super();

	        var order = self.pos.get_order();
	        this.set_default_client(order)
		},
		set_default_client: function(order){
	    	if (order) {
	    		var default_client = order.pos.config.default_customer_id
		    	order.set_client(this.pos.db.get_partner_by_id(default_client[0]))
	    	}
	    },
	});
});
