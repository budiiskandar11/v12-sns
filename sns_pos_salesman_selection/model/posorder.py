# -*- coding: utf-8 -*-
""" posorder """
from odoo import models, fields, api

class PosOrder(models.Model):
    """ class PosOrder """
    _inherit = "pos.order"

    salesperson = fields.Many2one('hr.employee', string='Sales Person')

    @api.model
    def _order_fields(self, ui_order):
        result = super(PosOrder, self)._order_fields(ui_order)
        result['salesperson'] = ui_order['salesperson']
        return result
