# -*- coding: utf-8 -*-
""" pos config """
from odoo import models, fields, api

class PosConfig(models.Model):
    """ class Pos Config """
    _inherit = "pos.config"

    default_customer_id = fields.Many2one('res.partner', string='Default Customer',
                                          domain=[('customer', '=', True)])
