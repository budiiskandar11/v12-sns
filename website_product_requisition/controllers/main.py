# -*- coding: utf-8 -*-
##########################################################################
#
#   Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE file for full copyright and licensing details.
#   License URL : <https://store.webkul.com/license.html/>
#
##########################################################################

import base64
import re

from odoo import http, tools, _
from odoo.http import request
from odoo.exceptions import ValidationError

import logging
_logger = logging.getLogger(__name__)

validPattern = "^(((?!0)|[-+]|(?=0+\.))(\d*\.)?\d+(e\d+)?)$"

class WebsiteSale(http.Controller):

    @http.route(['/request/product'], type='http', auth="public", website=True)
    def requisition(self, **post):
        values = {'submit_msg' : 'no'}
        userId = request.session.uid
        values1 = {}
        errors = {}
        errorMsg = {}
        if post.get('submitted') == "1":
            if post.get('avatar'):
                image = post.get('avatar').read()
                image = base64.b64encode(image)
                resized_images = tools.image_get_resized_images(image, return_big=True, avoid_resize_medium=True)
                values1['image'] = resized_images['image']
                values1['image_medium'] = resized_images['image_medium']
                values1['image_small'] = resized_images['image_small']
            if post.get('prod_name'):
                values1['name'] = post.get('prod_name')
            else:
                errors['prod_name'] = 'yes'
            prodQty = post.get('prod_qty')
            if prodQty:
                if re.match(validPattern, prodQty):
                    values1['prod_qty'] = prodQty
                else:
                    errors['prod_qty'] = 'yes'
                    errorMsg['prod_qty'] = 'Invalid Quantity! Please enter a valid quantity.'
            email = post.get('email')
            if userId:
                values1['customer_id'] = userId
            else:
                values1['guest_partner'] = 'Guest'
                errors['email'] = 'yes'
                if email:
                    if re.match("[^@]+@[^@]+\.[^@]+", email):
                        values1['guest_email'] = email
                        errors.pop('email')
                    else:
                        errorMsg['email'] = 'Invalid Email! Please enter a valid email.'
            if post.get('prod_desc'):
                values1['prod_description'] = post.get('prod_desc')
            if errors:
                values['submit_msg'] = 'no'
            elif values1.get('name'):
                reqProd = request.env['product.requisition'].sudo().create(values1)
                values['submit_msg'] = 'yes' 
        errors['error_message'] = errorMsg
        values.update({
            'error' : errors,
            'prod_name' : post.get('prod_name'),
            'email' : post.get('email'),
            'prod_qty' : post.get('prod_qty'),
            'prod_desc' : post.get('prod_desc'),
            'is_guest' : userId or 'yes',
        })
        return request.render("website_product_requisition.requisition", values)

    @http.route(['/my/requisitions/home'], type='http', auth="user", website=True)
    def my_requisitions_home(self, **post):
        reqProdModel = request.env['product.requisition'].sudo()
        userId = request.session.uid
        partner = request.env.user.partner_id
        noReq = False
        domain = ['|',
            ('message_partner_ids', 'child_of', [partner.commercial_partner_id.id]),
            ('customer_id', '=', userId),
        ]
        domainDone = domain + [('state', 'in', ['done', 'approve'])]
        domainPending = domain + [('state', '=', 'draft')]
        domainCancel = domain + [('state', '=', 'cancel')]
        reqProdDone = reqProdModel.search_count(domainDone)
        reqProdDraft = reqProdModel.search_count(domainPending)
        reqProdCancel = reqProdModel.search_count(domainCancel)
        if not reqProdDone and not reqProdDraft and not reqProdCancel:
            noReq = True
        values = {
            'my_req_count_pending' : reqProdDraft,
            'my_req_count_done' : reqProdDone,
            'my_req_count_cancel' : reqProdCancel,
            'no_requisition' : noReq,
        }
        return request.render("website_product_requisition.my_requisitions_home", values)

    @http.route(['/my/requisitions/pending'], type='http', auth="user", website=True)
    def my_requisitions_pending(self, **post):
        values = self.get_requisitions_data([('state', '=', 'draft')])
        return request.render("website_product_requisition.my_requisition_list", values)

    @http.route(['/my/requisitions/done'], type='http', auth="user", website=True)
    def my_requisitions_done(self, **post):
        values = self.get_requisitions_data([('state', 'in', ['done', 'approve'])])
        return request.render("website_product_requisition.my_requisition_list", values)

    @http.route(['/my/requisitions/cancel'], type='http', auth="user", website=True)
    def my_requisitions_cancel(self, **post):
        values = self.get_requisitions_data([('state', '=', 'cancel')])
        return request.render("website_product_requisition.my_requisition_list", values)

    def get_requisitions_data(self, newDomain):
        reqProdModel = request.env['product.requisition'].sudo()
        userId = request.session.uid
        partner = request.env.user.partner_id
        domain = ['|',
            ('message_partner_ids', 'child_of', [partner.commercial_partner_id.id]),
            ('customer_id', '=', userId),
        ]
        domain += newDomain
        requiObjs = reqProdModel.search(domain, order='create_date desc')
        published = {}
        for requiObj in requiObjs:
            prodTemp = requiObj.product_id
            published.update({requiObj.id : 'no'})
            if prodTemp and prodTemp.website_published:
                published.update({requiObj.id : 'yes'})
        vals = {
            'prod_req' : requiObjs.sudo(),
            'published' : published,
            'page_name' : 'requisiotion'
        }
        return vals
