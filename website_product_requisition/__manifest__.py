# -*- coding: utf-8 -*-
##########################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
##########################################################################

{
  'name'                 :  'Ecommerce New Product Requisition',
  'version'              :  '1.0.0',
  'category'             :  'website',
  'author'               :  'Webkul Software Pvt. Ltd.',
  'website'              :  'https://store.webkul.com/Odoo-Ecommerce-New-Product-Requisition.html',
  'sequence'             :  1,
  'summary'              :  '''
Do not let your product catalogue be the only options for your customers.
Expand your website by letting them speak their mind with
Odoo E-commerce New Product Requisition Module''',
  "live_test_url"        : "http://odoodemo.webkul.com/?module=website_product_requisition",
  'description'          :  """Ecommerce New Product Requisition """,
  'depends'              :  ['website_sale', 'sale_management'],
  'data'                 :  [
                              'security/prod_req.xml',
                              'security/ir.model.access.csv',
                              'data/mail_template_data.xml',
                              'views/product_requisition_view.xml',
                              'wizard/create_product_view.xml',
                              'wizard/cancel_requisition_view.xml',
                              'wizard/message_req_wizard_view.xml',
                              'views/req_template.xml',
                            ],
  "images"               :  ['static/description/Banner.png'],
  'application'          :  True,
  'installable'          :  True,
  'active'               :  False,
  "price"                :  35,
  "currency"             :  "EUR",
  'pre_init_hook'        :  'pre_init_check',
}
