# -*- coding: utf-8 -*-
##########################################################################
#
#   Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE file for full copyright and licensing details.
#   License URL : <https://store.webkul.com/license.html/>
#
##########################################################################
import logging

from odoo import api, fields, models
from odoo.exceptions import ValidationError
_logger = logging.getLogger(__name__)


class CreateProduct(models.TransientModel):
    _name = "create.product"

    def _get_product_types(self):
        return self.env['product.template'].fields_get(
            'type', 'selection').get('type', {}).get('selection',{})

    name = fields.Char(string='Name',
        help="Create Product by Updating Requested Name for Product")
    product_type = fields.Selection(
        _get_product_types, string="Type", default="consu"
    )
    prod_description = fields.Text(string="Product Description",
        help="Create Product by Updating  Requested Users/Customers Desciption for Product")

    @api.multi
    def create_product(self):
        return self.env['product.requisition'].create_product_now(
            self.name, self.prod_description, self.product_type)