# -*- coding: utf-8 -*-
##########################################################################
#
#   Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE file for full copyright and licensing details.
#   License URL : <https://store.webkul.com/license.html/>
#
##########################################################################

from odoo import _, api, fields, models


class CancelRequisition(models.TransientModel):
    _name = "cancel.requisition"

    cancel_reason = fields.Text(string='Reason', translate=True)

    @api.multi
    def cancel_requisition(self):
        ctx = dict(self._context or {})
        reqId = ctx.get('active_id')
        reqObj = self.env['product.requisition'].browse(reqId)
        reqObj.write({'state' : 'cancel', 'cancel_reason' : self.cancel_reason})
        reqObj.send_requisition_mail('email_template_requisition_cancel')
        text = 'Requisition successfully cancelled'
        partial = self.env['message.req.wizard'].create({'text': text})
        return {
            'name': ("Information"),
            'view_mode': 'form',
            'view_type': 'form',
            'res_model': 'message.req.wizard',
            'view_id': self.env.ref('website_product_requisition.message_req_wizard_form').id,
            'res_id': partial.id,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
        }
