# -*- coding: utf-8 -*-
##########################################################################
#
#   Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE file for full copyright and licensing details.
#   License URL : <https://store.webkul.com/license.html/>
#
##########################################################################

import operator
from odoo import api, fields, models, _
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import ValidationError

import logging
_logger = logging.getLogger(__name__)

class ProductRequisition(models.Model):
    _name = 'product.requisition'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = "Product Requisition"
    _order = 'create_date desc, id desc'


    name = fields.Char(string='Name', required=True, readonly=True, copy=False, index=True)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('approve', 'Approved'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),
        ], string='Status', readonly=True, copy=False, index=True, track_visibility='onchange', default='draft')

    prod_description = fields.Text(string="Product Description", readonly=True,
        help="Users/Customers can share the product desription")
    prod_qty = fields.Float(string="Requested Quantity",
        help="Users/Customers can request the product qunatity")
    product_id = fields.Many2one('product.template', string="Product", readonly=True)
    user_id = fields.Many2one('res.users', string="User", default=lambda self: self.env.user)
    customer_id = fields.Many2one('res.users', string="Request By", readonly=True,
        help="Customer who request the product")
    guest_partner = fields.Char(string="Request By", default='', readonly=True)
    guest_email = fields.Char(string="Email", readonly=True)
    cancel_reason = fields.Text(string="Reason", default='', readonly=True)
    image = fields.Binary(
        "Image", attachment=True,
        help="This field holds the image used as image for the product, limited to 1024x1024px.")
    image_medium = fields.Binary(
        "Medium-sized image", attachment=True,
        help="Medium-sized image of the product. It is automatically "
             "resized as a 128x128px image, with aspect ratio preserved, "
             "only when the image exceeds one of those sizes. Use this field in form views or some kanban views.")
    image_small = fields.Binary(
        "Small-sized image", attachment=True,
        help="Small-sized image of the product. It is automatically "
             "resized as a 64x64px image, with aspect ratio preserved. "
             "Use this field anywhere a small image is required.")
    

    @api.model
    def create(self, vals):
        reqObj = super(ProductRequisition, self).create(vals)
        if reqObj:
            reqObj.send_requisition_mail('email_template_requisition')
        return reqObj

    @api.multi
    def send_requisition_mail(self, templXmlId):
        self.ensure_one()
        irModelData = self.env['ir.model.data']
        templXmlId = irModelData.get_object_reference(
            'website_product_requisition', templXmlId)[1]
        baseUrl = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        redirectUrl = baseUrl + "/my/requisitions/home"
        emailto = self.guest_email or ''
        name = 'Guest'
        if self.customer_id:
            partnerObj = self.customer_id.partner_id
            emailto = partnerObj and partnerObj.email or ''
            name = self.customer_id.name
        ctx = {
            'wkemail' : emailto,
            'wkname' : name,
            'lang' : self._context.get('lang', 'en_US'),
            'redirectUrl' : redirectUrl,
        }
        mailTemplateModel = self.env['mail.template']
        mailTmplObj = mailTemplateModel.browse(templXmlId)
        mailTmplObj.with_context(**ctx).send_mail(self.id, force_send=True)

    @api.multi
    def approve_request(self):
        for obj in self:
            obj.send_requisition_mail('email_template_requisition_approve')
        self.write({'state' : 'approve'})

    @api.multi
    def cancel_request(self):
        self.ensure_one()
        ctx = dict(self._context or {})
        partial = self.env['cancel.requisition'].create({})
        return {
            'name': ("Create Product"),
            'view_mode': 'form',
            'view_id': False,
            'view_type': 'form',
            'res_model': 'cancel.requisition',
            'res_id': partial.id,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'context': ctx,
            'domain': '[]',
        }

    @api.multi
    def create_product(self):
        self.ensure_one()
        ctx = dict(self._context or {})
        vals = {
            'name' : self.name,
            'prod_description' : self.prod_description,
        }
        partial = self.env['create.product'].create(vals)
        return {
            'name': ("Create Product"),
            'view_mode': 'form',
            'view_id': False,
            'view_type': 'form',
            'res_model': 'create.product',
            'res_id': partial.id,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'context': ctx,
            'domain': '[]',
        }
        

    @api.model
    def create_product_now(self, name, desc, productType):
        ctx = dict(self._context or {})
        reqId = ctx.get('active_id')
        reqObj = self.browse(reqId)
        vals = {
            'name' : name,
            'description_sale' : desc,
            'type' : productType,
        }
        if reqObj.image:
            vals.update({
                'image' : reqObj.image,
                'image_medium' : reqObj.image_medium,
                'image_small' : reqObj.image_small,
            })
        tempObj = self.env['product.template'].sudo().create(vals)
        reqObj.write({
            'product_id' : tempObj.id,
            'state' : 'done',
        })
        text = 'Porduct successfully created'
        partial = self.env['message.req.wizard'].create({'text': text})
        return {
            'name': ("Information"),
            'view_mode': 'form',
            'view_type': 'form',
            'res_model': 'message.req.wizard',
            'view_id': self.env.ref('website_product_requisition.message_req_wizard_form').id,
            'res_id': partial.id,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
        }
