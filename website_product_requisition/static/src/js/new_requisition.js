odoo.define('website_product_requisition.requisition', function (require) {
    "use strict";

    require('web.dom_ready');
    var core = require('web.core');
    var _t = core._t;

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#req_image').attr('src', e.target.result);
                $('#req_image').css('display', 'inline');
            }

            reader.readAsDataURL(input.files[0]);
        } else {
            $('#req_image').css('display', 'none');
        }
    }

    $("#avatar").change(function () {
        readURL(this);
    });

});