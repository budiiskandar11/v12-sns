# -*- coding: utf-8 -*-
""" model """

from odoo import models, fields


class ProductProduct(models.Model):
    """ inherit account.payment """
    _inherit = "account.payment"

    payment_receive = fields.Date(string='Tanggal Terima')
