# -*- coding: utf-8 -*-
# Copyright (C) 2017 Portcities Indonesia (<http://portcities.net>).
# All Rights Reserved
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).
{
    "name": "SNS Custom Invoice Report",
    "summary": "Customization for Invoices",
    "version": "12.0.1.0.0",
    "author": "Portcities Indonesia",
    "license": "AGPL-3",
    "complexity": "normal",
    "description": """

v1.1
----
* Customize Invoice Report Template

*Author : Arimasendy W.

                      """,
    "category": "Reporting",
    "depends": [
        'base',
        'account',
    ],
    "data": [
        'reports/report_invoice.xml'
    ],
    "test": [
    ],
    # "external_dependencies": {"python" : ["num2words"]},
    "auto_install": False,
    'installable': True,
    "application": False,
}
