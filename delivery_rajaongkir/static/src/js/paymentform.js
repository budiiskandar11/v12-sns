odoo.define('delivery_rajaongkir.paymentform', function (require) {
    "use strict";

    var payment = require('payment.payment_form')

    payment.include({
        payEvent: function (ev) {
            ev.preventDefault();
            var jas = $('.list-group-item')
            var data = true
            for(var i = 0; i< jas.length; i++){
                if(jas[i].firstElementChild.checked == true){
                    data = false
                    break
                }
            }
            if(data){
                alert('please choose delivery method first !')
            }else{
                this._super(ev)
            }
        }
    })
});