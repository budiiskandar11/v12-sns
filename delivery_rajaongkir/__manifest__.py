# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Rajaongkir - Delivery Costs",
    'version': '11.0.1.0.0',
    'Summary': "Connector to Rajaongkir's service",
    'description':
    """
        1.0 Compute delivery price using starter account
            (Support to JNE, Tiki, and POS)
        @update:
        - set city field on web e-commerce account details (by Arimasendy W.)
    """,
    'category': 'Sales, Inventory',
    'author': 'Portcities Ltd',
    'website': 'https://portcities.net',
    'depends': ['portal', 'website_sale', 'delivery', 'mail', 'website_sale_delivery'],
    'data': [
        'data/delivery_rajaongkir_data.xml',
        "security/ir.model.access.csv",
        'views/delivery_rajaongkir_view.xml',
        'views/stock_report_deliveryslip.xml',
        'views/payment.xml',
        'views/templates.xml',
        'data/ir_cron_data.xml',
    ],
}
