# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import logging
from werkzeug.exceptions import Forbidden, NotFound
from odoo import http
from odoo.http import request, route
from odoo.addons.portal.controllers.portal import CustomerPortal
from odoo.addons.website_sale.controllers.main import WebsiteSale

_logger = logging.getLogger(__name__)

class CustomerPortals(CustomerPortal):
    MANDATORY_BILLING_FIELDS = ["name", "phone", "email", "street", "city", "city_id", "country_id"]
    OPTIONAL_BILLING_FIELDS = ["zipcode", "state_id", "vat", "company_name", "confirm_pwd", "new_password", "old_pwd"]

    @route(['/my/account'], type='http', auth='user', website=True)
    def account(self, redirect=None, **post):
        values = self._prepare_portal_layout_values()
        partner = request.env.user.partner_id
        values.update({
            'error': {},
            'error_message': [],
        })

        if post:
            error, error_message = self.details_form_validate(post)
            values.update({'error': error, 'error_message': error_message})
            values.update(post)
            if not error:
                values = {key: post[key] for key in self.MANDATORY_BILLING_FIELDS}
                values.update({key: post[key] for key in self.OPTIONAL_BILLING_FIELDS if key in post})
                values.update({'zip': values.pop('zipcode', '')})
                partner.sudo().write(values)
                if redirect:
                    return request.redirect(redirect)
                return request.redirect('/my/home')

        countries = request.env['res.country'].sudo().search([])
        states = request.env['res.country.state'].sudo().search([])
        statex = partner.state_id
        cities = request.env['res.country.city'].sudo().search([])

        values.update({
            'partner': partner,
            'countries': countries,
            'states': states,
            'statex': statex,
            'cities': cities,
            'has_check_vat': hasattr(request.env['res.partner'], 'check_vat'),
            'redirect': redirect,
            'page_name': 'my_details',
        })

        response = request.render("portal.portal_my_details", values)
        response.headers['X-Frame-Options'] = 'DENY'
        return response

    @route('/my/account_city', auth='public', methods=['GET'], website=False)
    def signup_city(self, **params):
        City = request.env['res.country.city'].sudo()
        try:
            state_id = int(params.get('state_id', False))
        except Exception:
            state_id = False
        content = 'select...'
        options = "<option value=''>%s</option>" % (content)
        if state_id == 0:
            content = 'No city for this state'
            options = "<option value='0'>%s</option>" % (content)
        else:
            if state_id:
                cities = City.search([('state_id', '=', state_id)])
                if cities:
                    for city in cities:
                        options += "<option value='%s'>%s</option>" % (city.id, city.name)
                else:
                    content = 'No city for this state'
                    options = "<option value='0'>%s</option>" % (content)
        return options
 
class WebsiteSales(WebsiteSale):
    def values_postprocess(self, order, mode, values, errors, error_msg):
        new_values = {}
        authorized_fields = request.env['ir.model']._get('res.partner')._get_form_writable_fields()
        for k, v in values.items():
            # don't drop empty value, it could be a field to reset
            if k == 'city_id' and v is not None:
                new_values['city_id'] = v
            if k in authorized_fields and v is not None:
                new_values[k] = v
            else:  # DEBUG ONLY
                if k not in ('field_required', 'partner_id', 'callback', 'submitted'): # classic case
                    _logger.debug("website_sale postprocess: %s value has been dropped (empty or not writable)" % k)
   
        new_values['customer'] = True
        new_values['team_id'] = request.website.salesteam_id and request.website.salesteam_id.id
   
        lang = request.lang if request.lang in request.website.mapped('language_ids.code') else None
        if lang:
            new_values['lang'] = lang
        if mode == ('edit', 'billing') and order.partner_id.type == 'contact':
            new_values['type'] = 'other'
        if mode[1] == 'shipping':
            new_values['parent_id'] = order.partner_id.commercial_partner_id.id
            new_values['type'] = 'delivery'
   
        return new_values, errors, error_msg
       
    @http.route(['/shop/address'], type='http', methods=['GET', 'POST'], auth="public", website=True)
    def address(self, **kw):
        Partner = request.env['res.partner'].with_context(show_address=1).sudo()
        order = request.website.sale_get_order()
   
        redirection = self.checkout_redirection(order)
        if redirection:
            return redirection
   
        mode = (False, False)
        def_country_id = order.partner_id.country_id
        def_state_id = order.partner_id.state_id
        def_city_id = order.partner_id.city_id
        values, errors = {}, {}
   
        partner_id = int(kw.get('partner_id', -1))
   
        # IF PUBLIC ORDER
        if order.partner_id.id == request.website.user_id.sudo().partner_id.id:
            mode = ('new', 'billing')
            country_code = request.session['geoip'].get('country_code')
            if country_code:
                def_country_id = request.env['res.country'].search([('code', '=', country_code)], limit=1)
            else:
                def_country_id = request.website.user_id.sudo().country_id
        # IF ORDER LINKED TO A PARTNER
        else:
            if partner_id > 0:
                if partner_id == order.partner_id.id:
                    mode = ('edit', 'billing')
                else:
                    shippings = Partner.search([('id', 'child_of', order.partner_id.commercial_partner_id.ids)])
                    if partner_id in shippings.mapped('id'):
                        mode = ('edit', 'shipping')
                    else:
                        return Forbidden()
                if mode:
                    values = Partner.browse(partner_id)
            elif partner_id == -1:
                mode = ('new', 'shipping')
            else: # no mode - refresh without post?
                return request.redirect('/shop/checkout')
   
        # IF POSTED
        if 'submitted' in kw:
            pre_values = self.values_preprocess(order, mode, kw)
            errors, error_msg = self.checkout_form_validate(mode, kw, pre_values)
            post, errors, error_msg = self.values_postprocess(order, mode, pre_values, errors, error_msg)
   
            if errors:
                errors['error_message'] = error_msg
                values = kw
            else:
                partner_id = self._checkout_form_save(mode, post, kw)
   
                if mode[1] == 'billing':
                    order.partner_id = partner_id
                    order.onchange_partner_id()
                elif mode[1] == 'shipping':
                    order.partner_shipping_id = partner_id
   
                order.message_partner_ids = [(4, partner_id), (3, request.website.partner_id.id)]
                if not errors:
                    return request.redirect(kw.get('callback') or '/shop/checkout')
   
        country = 'country_id' in values and values['country_id'] != '' and request.env['res.country'].browse(int(values['country_id']))
        country = country and country.exists() or def_country_id
        statex = 'state_id' in values and values['state_id'] != '' and request.env['res.country.state'].sudo().browse(int(values['state_id']))
        statex = statex and statex.exists() or def_state_id
        cities = request.env['res.country.city'].sudo().search([])
        render_values = {
            'website_sale_order': order,
            'partner_id': partner_id,
            'mode': mode,
            'checkout': values,
            'country': country,
            'countries': country.get_website_sale_countries(mode=mode[1]),
            "states": country.get_website_sale_states(mode=mode[1]),
            "statex": statex,
            "city_id": def_city_id.id,
            "cities": statex.sudo().city_ids,
            'error': errors,
            'callback': kw.get('callback'),
        }
        return request.render("website_sale.address", render_values)

    @http.route('/shop/city', auth='public', methods=['GET'], website=False)
    def signup_city(self, **params):
        City = request.env['res.country.city'].sudo()
        try:
            state_id = int(params.get('state_id', False))
        except Exception:
            state_id = False
        content = 'City...'
        options = "<option value=''>%s</option>" % (content)
        if state_id == 0:
            content = 'No city for this state'
            options = "<option value='0'>%s</option>" % (content)
        else:
            if state_id:
                cities = City.search([('state_id', '=', state_id)])
                if cities:
                    for city in cities:
                        options += "<option value='%s'>%s</option>" % (city.id, city.name)
                else:
                    content = 'No city for this state'
                    options = "<option value='0'>%s</option>" % (content)
        return options

    @http.route('/shop/state', auth='public', methods=['GET'], website=False)
    def signup_state(self, **params):
        state = request.env['res.country.state'].sudo()
        try:
            country_id = int(params.get('state_id', False))
        except Exception:
            country_id = False
        state_id= state.search([('country_id','=',country_id)], limit=1)
        if state_id:
            return str(state_id.id)
        else:            
            return 0

    @http.route('/shop/state1', auth='public', methods=['GET'], website=False)
    def signup_state1(self, **params):
        city = request.env['res.country.state'].sudo()
        try:
            state_id = int(params.get('state_id', False))
        except Exception:
            state_id = False
        state= city.search([('country_id','=',country_id)], limit=1)
        if state:
            return str(state.id)
        else:            
            return 0

    @http.route(['/shop/payment'], type='http', auth="public", website=True)
    def payment(self, **post):
        response = super(WebsiteSales, self).payment(**post)
        deliver_methode = request.env.user.partner_id.property_delivery_carrier_id
        if deliver_methode:
            if deliver_methode.delivery_type == 'fixed' and deliver_methode.fixed_price == 0:
                response.qcontext['deliveries'] = deliver_methode
        response.qcontext['data_partner'] = request.env.user.partner_id
        return response
