# -*- coding: utf-8 -*-
""" Delivery carrier """
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import models, fields, api
from .rajaongkir_request import RajaOngkirProvider


class RajaongkirAPI(models.Model):
    """ Define rajaongkir API key """

    _name = 'delivery.carrier.rajaongkirapi'
    _description = "Rajaongkir API Key"

    name = fields.Char('API Key Name')
    api_key = fields.Char("API Key")

class RajaongkirURL(models.Model):
    """ Define rajaongkir URL """
    _name = 'delivery.carrier.rajaongkirurl'
    _description = "Rajaongkir URL"

    name = fields.Char('URL Name')
    url = fields.Char("URL")

class ProviderRajaongkir(models.Model):
    """ Extend delivery carrier """
    _inherit = 'delivery.carrier'

    delivery_type = fields.Selection(selection_add=[('rajaongkir', "RajaOngkir")])

    rajaongkir_url = fields.Many2one('delivery.carrier.rajaongkirurl', 'URL')
    rajaongkir_apikey = fields.Many2one('delivery.carrier.rajaongkirapi', "API Key")
    rajaongkir_courier = fields.Selection([
        ('jne', 'JNE'),
        ('pos', 'POS'),
        ('tiki', 'TIKI'),
        ('rpx', 'RPX'),
        ('esl', 'ESL'),
        ('pcp', 'PCP'),
        ('pandu', 'Pandu Logistic'),
        ('wahana', 'Wahana'),
        ('sicepat', 'Si Cepat'),
        ('pahala', 'Pahala'),
        ('cahaya', 'Cahaya'),
        ('sap', 'SAP'),
        ('jet', 'Jet'),
        ('indah', 'Indah'),
        ('dse', 'DSE'),
        ('slis', 'SLIS'),
        ('first', 'First Logistic'),
        ('ncs', 'NCS'),
        ('star', 'Star')], default='jne', string='Courier')
    rajaongkir_service = fields.Char('Courier Services', help="Optional, leave blank")

    def rajaongkir_rate_shipment(self, orders):
        """ Compute delivery price """

        res = {}
        srm = RajaOngkirProvider(self.rajaongkir_url.url,
                                 self.rajaongkir_apikey.api_key, self.rajaongkir_courier)
        for order in orders:
            srm.check_required_value(self, order.partner_shipping_id,
                                     order.warehouse_id.partner_id, order=order)
            price = srm.rate_request(order, self)
            if order.currency_id.name != 'IDR':
                quote_currency = self.env['res.currency'].search([('name', '=', 'IDR')], limit=1)
                price = quote_currency.compute(float(price), order.currency_id)
            res.update({'price': price,
                        'success': True,
                        'warning_message': 'Info:\nPlease compare original price ' \
                                        'in https://rajaongkir.com'})
        return res

    def rajaongkir_get_tracking_link(self, pickings):
        """ Get delivery slip """

        res = []
        for picking in pickings:
            res = res + ['/report/pdf/stock.report_deliveryslip/%s' % picking.id]
        return res

    def rajaongkir_get_tracking_value(self, pickings):
        """ Get tracking value """

        res = []
        srm = RajaOngkirProvider(self.rajaongkir_url.url,
                                 self.rajaongkir_apikey.api_key, self.rajaongkir_courier)
        for picking in pickings:
            res = srm.track_request(picking.carrier_tracking_ref)
        return res

    def rajaongkir_sync_address(self):
        """ Sync address from rajaongkir """

        srm = RajaOngkirProvider(self.rajaongkir_url.url,
                                 self.rajaongkir_apikey.api_key, self.rajaongkir_courier)
        country_id = self.env['res.country'].search([('code', '=', 'ID')]).id
        state_query = []
        for state in srm._get_all_states():
            state_query.append("('%s', %s, %s, NOW() AT TIME ZONE 'UTC'," \
                               "NOW() AT TIME ZONE 'UTC', 1, 1)" \
                               % (state['province'], state['province_id'], country_id))
        self._cr.execute("""
            INSERT INTO res_country_state AS d (name, code, country_id, create_date,
                        write_date, create_uid, write_uid) values %s
            ON CONFLICT(code, country_id) DO UPDATE SET
                name = EXCLUDED.name,
                write_date = NOW() AT TIME ZONE 'UTC',
                write_uid=1 RETURNING name, id""" % ', '.join(state_query))
        self._cr.execute("""SELECT name, id FROM res_country_state WHERE country_id = %s""" \
                         % country_id)
        states = dict(self._cr.fetchall())

        city_query = []
        cities_query = srm._get_all_cities()
        for city in cities_query:
            city_query.append("('%s', %s, %s, NOW() AT TIME ZONE 'UTC'," \
                              "NOW() AT TIME ZONE 'UTC', 1, 1)" \
                              % ('%s %s' % (city['type'], city['city_name'],),
                                 city['city_id'], states[city['province']]))

        self._cr.execute("""
            INSERT INTO res_country_city AS d (name, rajaongkir_id, state_id, create_date, write_date,
                                               create_uid, write_uid) values %s
            ON CONFLICT(rajaongkir_id) DO UPDATE SET
                name = EXCLUDED.name,
                state_id = EXCLUDED.state_id,
                write_date = NOW() AT TIME ZONE 'UTC',
                write_uid=1""" % ', '.join(city_query))

        self._cr.execute("""SELECT rajaongkir_id, id FROM res_country_city""")
        cities = self._cr.dictfetchall()

        # sub district only available in pro account
        if 'pro' in self.rajaongkir_url.url:
            subdistrict_query = []
            for city in cities_query:
                for subdistrict in srm._get_subdistrict(city['city_id']):
                    city_id = filter(lambda x: x['rajaongkir_id'] == \
                                     int(subdistrict['city_id']), cities)[0]['id']

                    subdistrict_query.append("('%s', %s, %s, NOW() AT TIME ZONE 'UTC'," \
                                             "NOW() AT TIME ZONE 'UTC', 1, 1)" \
                                             % (subdistrict['subdistrict_name'].replace("'", "''"),
                                                subdistrict['subdistrict_id'], city_id))

            self._cr.execute("""
                INSERT INTO res_country_subdistrict AS d (name, rajaongkir_id, city_id, create_date, write_date,
                                                          create_uid, write_uid) values %s
                ON CONFLICT(rajaongkir_id) DO UPDATE SET
                    name = EXCLUDED.name,
                    city_id = EXCLUDED.city_id,
                    write_date = NOW() AT TIME ZONE 'UTC',
                    write_uid=1""" % ', '.join(subdistrict_query))

    @api.model
    def _rajaongkir_sync_address(self):
        """ Sync Address """

        rajaongkir_carrier = self.search([('delivery_type', '=', 'rajaongkir')], limit=1)
        if rajaongkir_carrier:
            rajaongkir_carrier.rajaongkir_sync_address()
