# -*- coding: utf-8 -*-
""" Partner localization """
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import models, fields, api


class ResPartner(models.Model):
    """ Extend partner"""
    _inherit = 'res.partner'

    subdistrict_id = fields.Many2one('res.country.subdistrict', 'Subdistrict')
    subdistrict = fields.Char(related='subdistrict_id.name', store=True)
    city_id = fields.Many2one('res.country.city', 'Subdistrict')
    city = fields.Char(related='city_id.name', store=True)

    @api.model
    def _address_fields(self):
        """Returns the list of address fields that are synced from the parent."""

        res = super(ResPartner, self)._address_fields()
        res += 'subdistrict',
        return res

    @api.onchange('state_id')
    def _onchange_state_id(self):
        if self.state_id:
            return {'domain': {'city_id': [('state_id', '=', self.state_id.id)]}}
        else:
            return {'domain': {'city_id': []}}

class ResCountryState(models.Model):
    """ Define Country State """

    _inherit = 'res.country.state'

    city_ids = fields.One2many('res.country.city', 'state_id', 'Cities')


class ResCountrySubdistrict(models.Model):
    """ Define Sub District """

    _name = 'res.country.subdistrict'
    _description = 'Subdistrict'

    name = fields.Char('Subdistrict', required=True)
    city_id = fields.Many2one('res.country.city', 'City', required=True)
    rajaongkir_id = fields.Integer('Rajaongkir Ref', index=True)

    _sql_constraints = [
        ('unique_rajaongkir_subdistrict',
         'unique(rajaongkir_id)',
         "Rajaongkir Ref Should be Unique!"),
    ]


class ResCountryCity(models.Model):
    """Define City"""

    _name = 'res.country.city'
    _description = 'City'

    name = fields.Char('City', required=True)
    state_id = fields.Many2one('res.country.state', 'State', required=False)
    rajaongkir_id = fields.Integer('Rajaongkir Ref', index=True)
    subdistrict_ids = fields.One2many('res.country.subdistrict', 'city_id', 'Subdistricts')

    _sql_constraints = [
        ('unique_rajaongkir_city', 'unique(rajaongkir_id)', "Rajaongkir Ref Should be Unique!"),
    ]
