# -*- coding: utf-8 -*-
""" Delivery tracker """
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import models, api


class StockPicking(models.Model):
    """ Extend stock picking"""

    _inherit = 'stock.picking'

    @api.multi
    def _track_rajaongkir(self):
        """ Get tracking value """

        self.ensure_one()
        try:
            vals = self.carrier_id.rajaongkir_get_tracking_value(self)
            res = {
                'destination' : vals['summary']['destination'],
                'status' : vals['summary']['status'],
                'manifest' : [{
                    'code' : m['manifest_code'],
                    'description' : m['manifest_description'],
                    'date' : m['manifest_date'],
                    'time' : m['manifest_time'],
                    'city' : m['city_name'],
                } for m in vals['manifest']],
                'delivered' : vals['delivered'],
                'receiver_name' : vals['summary']['receiver_name']
            }
        except:
            return False
        return res
