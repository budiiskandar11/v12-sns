# -*- coding: utf-8 -*-
""" Sale Order """
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import models, api, fields


class SaleOrder(models.Model):
    """ Extend sale order"""
    _inherit = 'sale.order'

    carrier_id = fields.Many2one('delivery.carrier',
                                 string="Delivery Method",
                                 required=True,
                                 help="Fill this field if you plan to invoice the shipping based on picking.")
