
#  -*- coding: utf-8 -*-
#################################################################################
#
#   Copyright (c) 2018-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE URL <https://store.webkul.com/license.html/> for full copyright and licensing details.
#################################################################################
import logging
from odoo import http
from odoo.http import request
from odoo.tools.translate import _
import werkzeug.utils
from odoo.addons.web.controllers.main import  Home
_logger = logging.getLogger(__name__)
class Home(Home):

    @http.route('/web/email/verification', type='http', auth="none")
    def web_email_verification(self, redirect=None, **kw):
        res = request.env['res.users'].wk_verify_email(kw)
        return request.render('email_verification.email_verification_template',{'status':res['status'],'msg':res['msg']})
    
    @http.route('/resend/email', type='http', auth='public', website=True)
    def resend_email(self, *args, **kw):
        request.env['res.users'].sudo().send_verification_email(request.uid)
        return 