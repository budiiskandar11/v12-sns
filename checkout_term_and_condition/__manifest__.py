# -*- coding: utf-8 -*-
# Part of AppJetty. See LICENSE file for full copyright and licensing details.

{
    'name': 'Checkout Terms and Conditions',
    'description': 'Checkout Terms and Conditions',
    'summary': 'Add Customizable Terms and Conditions on your checkout page, Avoid Sale Dispute',
    'category': 'Sales',
    'version': '12.0.1.0.0',
    'author': 'AppJetty',
    'website': 'https://goo.gl/sLNOeF',
    'support': 'support@appjetty.com',
    'depends': ['website_sale'],
    'data': [
        'views/website_config_view.xml',
        'views/checkout_template_view.xml',
    ],
    'images': ['static/description/splash-screen.png'],
    'price': 15.00,
    'currency': 'EUR',
    'installable': True,
}
