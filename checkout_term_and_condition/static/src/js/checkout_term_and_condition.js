odoo.define('checkout_term_and_condition.term', function(require) {
    "use strict";

    var ajax = require('web.ajax');
    $(document).ready(function() {

        if ($("#term_cond").length > 0){
            if ($('#term_cond').is(":checked") === false) {
                $("button#o_payment_form_pay").prop("disabled", true);
            }
            if ($("#term_cond").length) {
                $("#term_cond").click(function() {
                    $("button#o_payment_form_pay").prop("disabled", !this.checked);
                });
            }
        }
        $(window).bind('load',function(){
            setTimeout(function(){
                var check = document.getElementById('term_cond').checked;
                if(!check){
                    $('#o_payment_form_pay').prop('disabled', true);
                }
            },200);
        });
        $('.list-group').on('click', function() {
            setTimeout(function() {
                var chk = document.getElementById('term_cond').checked;
                if (chk) {
                    $('#o_payment_form_pay').prop('disabled', false);
                } else {
                    $('#o_payment_form_pay').prop('disabled', true);
                }
            }, 300);
        });
    });
});