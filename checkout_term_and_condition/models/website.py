# -*- coding: utf-8 -*-
# Part of AppJetty. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class website(models.Model):

    """Adds the fields for Terms and Condition."""

    _inherit = 'website'

    term = fields.Text(string='Terms and Conditions Content',
                       translate=True)
    label = fields.Char(string="Terms and Conditions Label",
                        default="I accept the", translate=True,
                        help="The Label of the Terms and conditions")
    label_popup = fields.Char(
        string="Hyper Link content",
        default="Terms and Conditions",
        help="Hyper Link label to open Terms and Conditions Pop Up",
        translate=True)
    is_label_popup = fields.Boolean(
        string="Disable Pop Up for Terms and Conditions",
        default=False,)
    is_header = fields.Boolean(string="Enable Title",
                               default=False)
    header = fields.Char(string="Title", default="Terms and Conditions",
                         help="If Title is enabled", translate=True)
    is_terms_condition_feature = fields.Boolean(
        string="Disable Terms and Conditions feature ",
        defalut=False,)
    saledefault_term_condition = fields.Boolean(
        string='Default Checkbox enabled for Terms and Conditions',
        default=True)

    @api.onchange('is_terms_condition_feature')
    def label_change(self):
        if self.is_terms_condition_feature:
            self.is_header = False
            self.is_label_popup = True
        else:
            self.is_label_popup = False

    @api.onchange('is_header')
    def label_change1(self):
        if self.is_header == False:
            self.header = False

    @api.onchange('is_label_popup')
    def label_popup_change(self):
        if self.is_label_popup:
            self.label_popup = False

    @api.multi
    def write(self, vals):
        res = super(website, self).write(vals)
        for rec_website in self:
            if not rec_website.label or rec_website.label and rec_website.label.isspace():
                raise UserError(
                    _('Please Enter some content in Terms and Conditions Label only space are not allow'))

            if rec_website.is_label_popup == False:
                if rec_website.label:
                    if rec_website.label_popup == False:
                        raise UserError(
                            _('Please Enter Hyperlink Content to show at Front End'))
                if rec_website.label_popup:
                    if rec_website.label == False:
                        raise UserError(
                            _('Please Enter Terms and Conditions Label to show at Front End'))
        return res


class ResConfigSettings(models.TransientModel):

    """Settings for the Terms and Condition."""

    _inherit = 'res.config.settings'

    term = fields.Text(
        related='website_id.term',
        string='Terms and Conditions Content',
        store=True, translate=True, readonly=False
    )

    is_header = fields.Boolean(
        related='website_id.is_header',
        string="Enable Title", readonly=False)

    label = fields.Char(
        related="website_id.label",
        string='Terms and Conditions Label',
        store=True, translate=True, readonly=False)

    label_popup = fields.Char(
        related="website_id.label_popup",
        string="Hyper Link content",
        store=True, translate=True, readonly=False)

    is_label_popup = fields.Boolean(
        related='website_id.is_label_popup',
        string="Disable Pop Up for Terms and Conditions ", readonly=False)
    header = fields.Char(
        related="website_id.header",
        string="Title",
        store=True, translate=True, readonly=False)
    is_terms_condition_feature = fields.Boolean(
        related='website_id.is_terms_condition_feature',
        string="Disable Terms and Conditions feature ", readonly=False)

    saledefault_term_condition = fields.Boolean(
        related="website_id.saledefault_term_condition",
        string='Default Checkbox enabled for Terms and Conditions', readonly=False
    )
