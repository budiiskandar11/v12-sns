from odoo import api, fields, models
from odoo.exceptions import ValidationError


class SaleOrder(models.Model):
    """ inherited sale.order object """
    _inherit = "sale.order"

    @api.multi
    def change_action_confirm(self):
        for order in self:
            manager = self.user_has_groups('account.group_account_manager')
            limit = order.amount_total > order.partner_id.remaining_limit
            print(order.partner_id.active_credit_limit, limit, manager)
            if order.partner_id.active_credit_limit and limit and not manager :
                raise ValidationError("Only Finance Manager Can Be Confirmed")
        return super(SaleOrder, self).change_action_confirm()
