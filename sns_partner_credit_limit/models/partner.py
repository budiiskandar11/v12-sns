from odoo import api, fields, models
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF


class ResPartner(models.Model):
    """ inherited res_partner object """
    _inherit = "res.partner"

    credit_limit = fields.Float(string='Credit Limit', track_visibility='onchange')
    current_credit = fields.Float("Current Credit", compute='_compute_current_credit')
    over_due = fields.Selection(compute='_compute_credit', string='Status Invoice',
                             selection=[('not_overdue', 'Not Over Due'), ('overdue', 'Over Due')])
    remaining_limit = fields.Float("Remaining Limit", compute='_compute_credit')
    active_credit_limit = fields.Boolean(string='Active Credit Limit')


    def _compute_current_credit(self):
        """ this function to calculate credits,
        1. Take all amount from journal payable that not paid
        """
        credits = 0.0
        for self_id in self:
            partner = self_id.id
            moveline_obj = self_id.env['account.move.line'].sudo()
            movelines = moveline_obj.search(
                [('partner_id', '=', partner), ('full_reconcile_id', '=', False),
                 ('account_id.user_type_id.name', '=', 'Receivable')]
            )
            acc_debit, acc_credit = 0.0, 0.0
            for line in movelines:
                acc_credit += line.debit
                acc_debit += line.credit
            credits = acc_credit - acc_debit
            self_id.current_credit = credits
    
    @api.multi
    def _compute_credit(self):
        """ 
            this function to give information over due
            this function to give information remaining credit
        """
        for self_id in self:
            partner = self_id.id
            self_id.over_due = 'not_overdue'
            moveline_obj = self_id.env['account.move.line'].sudo()
            today_dt = datetime.strftime(datetime.now().date(), DF)
            movelines = moveline_obj.search(
                [('partner_id', '=', partner), ('full_reconcile_id', '=', False),
                 ('account_id.user_type_id.name', 'in', ['Receivable', 'Payable']),
                 ('date_maturity', '<', today_dt)]
            )
            if movelines:
                self_id.over_due = 'overdue'
            date = datetime.strptime(today_dt, "%Y-%m-%d")
            if self_id.active_credit_limit:
                self_id.remaining_limit = self_id.credit_limit - self_id.current_credit

    