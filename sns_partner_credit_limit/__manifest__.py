{
    'name': 'Partner - Credit Limit, Bad Debt and Overdue',
    'version': '12.0.0.1.0',
    'category': 'Contact',
    'summary': 'Information Credit Limit, Bad Debt and Overdue',
    'author': 'Port Cities',
    'website': 'http://portcities.net',
    'description': """
    v 1.0
        author : Veri \n
        * Information Credit Limit, Bad Debt and Overdue 
    """,
    'depends': ['account', 'contacts', 'sale', 'bi_sale_tripple_approval'],
    'data': [
        'views/partner_view.xml',
        'views/partner_template.xml',
    ],
    'active': False,
    'installable': True,
    'application': False,
    'auto_install': False
}
