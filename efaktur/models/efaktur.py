from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError


class EFaktur(models.Model):
    _name = "efaktur"
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin']
    _description = "E-Faktur"
    _order = "date_assigned desc, company_id asc, name asc"

    name = fields.Char(
        string='Reference/Description', index=True, copy=False,
        size=20,
        help="Kode  dan  nomor  seri  Faktur  Pajak  terdiri  dari  16  digit. \n"
        "2 Digit pertama adalah TIN (Tax Identification Number) / Kode Transaksi Faktur Pajak. \n"
        "1 Digit berikutnya adalah Kode Kategori Faktur Pajak, dimana 0 sebagai Faktur Normal \n"
        "dan 1 sebagai Faktur Pengganti. \n"
        "13 Digit berikutnya adalah Nomor Seri Faktur Pajak yang ditentukan oleh DJP. \n"
        "Dimana 3 Digit pertama adalah sebagai Nomor Head Seri di pisah 2 Digit Tahun \n"
        "dan 11 Digit terakhir adalah Nomor Tail Seri Faktur Pajak.")
    number = fields.Char(
        string='Nomor Seri Faktur Pajak',
        copy=False,
        size=15,
        help="13 Digit Nomor Seri Faktur Pajak yang ditentukan oleh DJP"
    )
    date_assigned = fields.Date(string='Assigned Date', change_default=True)
    date_published = fields.Date(string='Tanggal Terbit Pajak', change_default=True)
    tin = fields.Selection([
        ('01', '01 Kepada Pihak yang Bukan Pemungut PPN (Customer Biasa)'),
        ('02', '02 Kepada Pemungut Bendaharawan (Dinas Kepemerintahan)'),
        ('03', '03 Kepada Pemungut Selain Bendaharawan (BUMN)'),
        ('04', '04 DPP Nilai Lain (PPN 1%)'),
        ('06', '06 Penyerahan Lainnya (Turis Asing)'),
        ('07', '07 Penyerahan yang PPN-nya Tidak Dipungut (Kawasan Ekonomi Khusus/ Batam)'),
        ('08', '08 Penyerahan yang PPN-nya Dibebaskan (Impor Barang Tertentu)'),
        ('09', '09 Penyerahan Aktiva ( Pasal 16D UU PPN )'),
    ], string='Tax Identification Number', inverse='_inverse_name',
        help='Tax Idenfication Number (Kode Transaksi Faktur Pajak)',
        track_visibility='onchange')
    category = fields.Selection([
        ('0', 'Faktur Normal'),
        ('1', 'Faktur Pengganti'),
    ], inverse='_inverse_name', string='Kategori Faktur Pajak', default='0')
    type = fields.Selection([
        ('sale', 'Faktur Pajak Keluaran'),
        ('purchase', 'Faktur Pajak Masukkan'),
    ], string='Tipe Faktur Pajak', index=True)
    state = fields.Selection([
        ('draft', 'New'),
        ('assign', 'Assigned'),
        ('submit', 'Submitted'),
        ('done', 'Verified'),
        ('cancel', 'Cancelled'),
    ], string='Status', index=True, inverse='_inverse_name', default='draft',
        track_visibility='onchange', copy=False,
        help=" * The 'Draft' status is used when a user is encoding a new "
        "and unused to invoice.\n"
        " * The 'Assigned' status is used when user already assigned "
        "efaktur to an invoice.\n"
        " * The 'Verified' status is set when efaktur is already "
        "verified on Indonesian Tax System. Its need to attached the "
        "document flow.\n"
        " * The 'Cancelled' status is used when user cancel invoice.")
    replacement_id = fields.Many2one(
        'efaktur',
        string='Replacement of Faktur Pajak',
        readonly=True,
        index=True,
    )
    invoice_id = fields.Many2one(
        'account.invoice',
        inverse='_inverse_assigned_invoice',
        string='Invoice', change_default=True, readonly=True,
        track_visibility='always'
    )
    date_invoice = fields.Date(
        string='Invoice Date', change_default=True)
    partner_id = fields.Many2one(
        'res.partner', string='Partner', change_default=True,
    )
    npwp = fields.Char(string='NPWP', readonly=True)
    ktp = fields.Char(string='KTP', readonly=True)
    currency_id = fields.Many2one(
        'res.currency', string='Currency',
    )
    amount_untaxed = fields.Monetary(
        string='Untaxed Amount', currency_field='currency_id',
        store=True, related='invoice_id.amount_untaxed', track_visibility='onchange')
    amount_tax = fields.Monetary(
        string='Tax Amount', currency_field='currency_id',
        store=True, related='invoice_id.amount_tax', track_visibility='onchange')
    company_id = fields.Many2one(
        'res.company', string='Company', change_default=True,
        required=True, readonly=True, states={'draft': [('readonly', False)]},
        default=lambda self: self.env.user.company_id.id
    )

    @api.constrains('name')
    def _check_unique_constraint(self):
        self.ensure_one()
        domain = [
            # ('name', 'ilike', self.name),
            ('tin', '=', self.tin),
            ('category', '=', self.category),
            ('number', '=', self.number),
            ('company_id', '=', self.env.user.company_id.id),
            ('type', '=', self.type)
        ]
        if len(self.search(domain, limit=2)) > 1:
            raise ValidationError(_("Nomor Seri Faktur Pajak is Already Exist!"))

    def _prepare_update_vals_on_assigned_invoice(self, detach=False):
        vals = {
            'date_invoice': self.invoice_id.date_invoice if not detach else False,
            'partner_id': self.invoice_id.partner_id if not detach else False,
            'tin': self.invoice_id.partner_id.vat if not detach else False,
            'npwp': self.invoice_id.partner_id.npwp if not detach else False,
            'ktp': self.invoice_id.partner_id.ktp if not detach else False,
            'currency_id': self.invoice_id.currency_id if not detach else False,
            'date_assigned': fields.Date.context_today(self) if not detach else False,
            'state': 'assign' if not detach and self.invoice_id.state != 'draft' else 'draft',
        }

        return vals

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        def get_view_id(xid, name):
            try:
                return self.env.ref('efaktur.' + xid)
            except ValueError:
                view = self.env['ir.ui.view'].search([('name', '=', name)], limit=1)
                if not view:
                    return False
                return view.id

        context = self._context
        if context.get('active_model') == 'res.partner' and context.get('active_ids'):
            partner = self.env['res.partner'].browse(context['active_ids'])[0]
            if not view_type:
                view_id = get_view_id('efaktur_sale_view_tree', 'efaktur.sale.tree.view')
                view_type = 'tree'
            elif view_type == 'form':
                if partner.supplier and not partner.customer:
                    view_id = get_view_id('efaktur_purchase_view_form',
                                          'efaktur.purchase.form.view')
                elif partner.customer and not partner.supplier:
                    view_id = get_view_id('efaktur_sale_view_form', 'efaktur.sale.form.view')
            elif view_type == 'tree':
                if partner.supplier and not partner.customer:
                    view_id = get_view_id('efaktur_purchase_view_tree',
                                          'efaktur.purchase.tree.view')
                elif partner.customer and not partner.supplier:
                    view_id = get_view_id('efaktur_sale_view_tree', 'efaktur.sale.tree.view')
        return super(EFaktur, self).fields_view_get(view_id=view_id, view_type=view_type,
                                                    toolbar=toolbar, submenu=submenu)

    @api.multi
    @api.depends('tin')
    def _inverse_name(self):
        for efaktur in self:
            if not efaktur.tin or efaktur.state in ('draft', 'cancel'):
                efaktur.name = efaktur.number
            else:
                efaktur.name = "%s%s-%s" % (
                    efaktur.tin, efaktur.category, efaktur.number
                )

    @api.depends('invoice_id')
    def _inverse_assigned_invoice(self):
        self.ensure_one()
        no_invoice = self.filtered(lambda efaktur: not efaktur.invoice_id)
        if not no_invoice:
            has_invoice = self.filtered(lambda efaktur: efaktur.invoice_id)
            for efaktur in has_invoice:
                if not efaktur.invoice_id.partner_id.vat \
                    and efaktur.invoice_id.type in ('out_invoice', 'out_refund'):
                    raise ValidationError(_("There is no "
                                            "Kode Transaksi Faktur Pajak on the %s!" % (
                                                'Customer' if efaktur.type == 'sale' else 'Vendor')))

                data = efaktur._prepare_update_vals_on_assigned_invoice(detach=False)
                efaktur.update(data)
        else:
            for efaktur in no_invoice:
                data = efaktur._prepare_update_vals_on_assigned_invoice(detach=True)
                efaktur.update(data)

    @api.multi
    def action_efaktur_cancel(self):
        self.ensure_one()
        if self.env.context.get('manual_cancel'):
            self.invoice_id.with_context({'manual_cancel': True}).action_invoice_cancel()

        return self.update({
            'state': 'cancel'
        })

    @api.multi
    def action_efaktur_reset(self):
        self.ensure_one()
        if self.env.context.get('manual_reset'):
            self.invoice_id.with_context({'manual_reset': True}).action_invoice_draft()

        return self.update({
            'invoice_id': False,
            'state': 'draft'
        })

    @api.multi
    def action_efaktur_replace(self):
        self.ensure_one()
        current_efaktur = self.filtered(lambda efaktur: efaktur.state == 'cancel')
        if not current_efaktur:
            raise UserError(_("E-faktur must be in cancel state when want to "
                              "create of replacement."))
        new = current_efaktur.copy()
        new.update({
            'type': current_efaktur.type,
            'category': '1',
            'replacement_id': current_efaktur.id,
        })
        return new
