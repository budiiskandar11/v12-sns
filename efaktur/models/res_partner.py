from odoo import models, fields, api, _


class ResPartner(models.Model):
    _inherit = "res.partner"

    tin_manual = fields.Selection([
        ('01', '01 Kepada Pihak yang Bukan Pemungut PPN (Customer Biasa)'),
        ('02', '02 Kepada Pemungut Bendaharawan (Dinas Kepemerintahan)'),
        ('03', '03 Kepada Pemungut Selain Bendaharawan (BUMN)'),
        ('04', '04 DPP Nilai Lain (PPN 1%)'),
        ('06', '06 Penyerahan Lainnya (Turis Asing)'),
        ('07', '07 Penyerahan yang PPN-nya Tidak Dipungut (Kawasan Ekonomi Khusus/ Batam)'),
        ('08', '08 Penyerahan yang PPN-nya Dibebaskan (Impor Barang Tertentu)'),
        ('09', '09 Penyerahan Aktiva ( Pasal 16D UU PPN )'),
        ], string='Tax Idenfication Number (Manual)', inverse='_inverse_assigned_tin',
                                  help='Tax Idenfication Number (Kode Transaksi Faktur Pajak)',
                                  track_visibility='onchange'
                                 )
    npwp = fields.Char(string='NPWP')
    ktp = fields.Char(string='KTP')

    @api.depends('tin_manual')
    def _inverse_assigned_tin(self):
        self.ensure_one()
        if not self.tin_manual:
            self.update({
                'vat': '',
            })
        else:
            self.update({
                'vat': self.tin_manual
            })
