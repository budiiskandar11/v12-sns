from odoo import models, fields, api, _
from openerp.exceptions import ValidationError


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    npwp = fields.Char(string='NPWP', related='partner_id.npwp', store=True,)
    ktp = fields.Char(string='KTP', related='partner_id.ktp', store=True,)
    tax_reference = fields.Char(string='Vendor Tax Reference',
                                inverse='_inverse_assigned_efaktur', copy=False)
    efaktur_id = fields.Many2one(
        'efaktur',
        string='Nomor EFaktur',
        inverse='_inverse_assigned_efaktur',
        copy=False
    )
    replacement_id = fields.Many2one(
        'account.invoice',
        string='Replacement of Invoice',
        domain="[('state', '=', 'cancel')]",
        index=True,
    )
    efaktur_state = fields.Selection([
        ('draft', 'New'),
        ('assign', 'Assigned'),
        ('submit', 'Submitted'),
        ('done', 'Verified'),
        ('cancel', 'Cancelled'),
    ], string='E-Faktur Status', default='draft', track_visibility='onchange',
        help=" * The 'Draft' status is used when a"
        "user is encoding a new and unused to invoice.\n"
        " * The 'Assigned' status is used when user"
        "already assigned efaktur to an invoice.\n"
        " * The 'Verified' status is set when efaktur is "
        "already verified on Indonesian Tax System. Its "
        "need to attached the document flow.\n"
        " * The 'Cancelled' status is used when user "
        "cancel invoice.")

    def _prepare_efaktur_vals(self):
        vals = {
            'number': self.tax_reference,
            'invoice_id': self.id,
            'date_published': self.date_invoice,
            'date_assigned': fields.Date.context_today(self),
            'date_invoice': self.date_invoice,
            'state': 'assign',
            'type': 'sale' if self.type in ('out_invoice', 'out_refund') else 'purchase'
        }
        return vals

    @api.multi
    def name_get(self):
        TYPES = {
            'out_invoice': _('Invoice'),
            'in_invoice': _('Vendor Bill'),
            'out_refund': _('Credit Note'),
            'in_refund': _('Vendor Credit note'),
        }
        res = super(AccountInvoice, self).name_get()
        for cancel in self.filtered(lambda inv: inv.state == 'cancel'):
            res.append((cancel.id, "Cancelled %s %s" % (
                cancel.move_name or TYPES[cancel.type],
                cancel.name or ''
            )))
        return res

    @api.depends('efaktur_id')
    def _inverse_assigned_efaktur(self):
        for bill in self.filtered(
            lambda r: not r.efaktur_id and r.tax_reference
                and r.type in ('in_invoice', 'in_refund')):
            reference_exist = self.env['efaktur'].search(
                [('number', '=', bill.tax_reference)], limit=1)
            if not reference_exist:
                vals = bill._prepare_efaktur_vals()
                efakturs = self.env['efaktur'].create(vals)
                bill.update({
                    'efaktur_id': efakturs,
                    'efaktur_state': efakturs.state})
            else:
                if not reference_exist.invoice_id:
                    if reference_exist.type == 'purchase':
                        bill.update({
                            'efaktur_id': reference_exist.id,
                            'efaktur_state': reference_exist.state
                            })
                    else:
                        raise ValidationError(_(
                            "'Nomor Seri Faktur Pajak' exist in 'Faktur Pajak Keluaran'!"
                        ))
                else:
                    raise ValidationError(_(
                        "'Nomor Seri Faktur Pajak' has been used on other Bill!"
                        ))

        for invoice in self.filtered(lambda r: r.efaktur_id and not r.efaktur_id.invoice_id):
            manual_replacement_invoice = self.env.context.get('manual_replacement')
            if not manual_replacement_invoice:
                invoice.efaktur_id.update({'invoice_id': invoice.id,})
                invoice.update({
                    'efaktur_state': self.efaktur_id.state,
                })

    @api.multi
    def action_invoice_cancel(self):
        res = super(AccountInvoice, self).action_invoice_cancel()
        manual_cancel_efaktur = self.env.context.get('manual_cancel')
        if not manual_cancel_efaktur:
            efaktur_id = self.efaktur_id.filtered(
                lambda efaktur: efaktur.state not in ['draft', 'done'])
            if efaktur_id:
                efaktur_id.action_efaktur_cancel()
        return res

    @api.multi
    def action_invoice_draft(self):
        res = super(AccountInvoice, self).action_invoice_draft()
        manual_reset_efaktur = self.env.context.get('manual_reset')
        if not manual_reset_efaktur and self.efaktur_id:
            efaktur_id = self.efaktur_id
            efaktur_id.action_efaktur_reset()
        self.update({'efaktur_id': False, })
        return res

    @api.multi
    def action_invoice_open(self):
        res = super(AccountInvoice, self).action_invoice_open()
        if self.filtered(lambda inv: inv.efaktur_id):
            self.efaktur_id.update({'state': 'assign'})
        return res

    @api.multi
    def action_invoice_replace(self):
        ctx = self._context
        view_id = self.env.ref('account.invoice_supplier_form') \
            if ctx.get('type') in ('in_invoice', 'in_refund') else \
            self.env.ref('account.invoice_form')

        current_invoice = self.filtered(lambda inv: inv.state == 'cancel')
        efaktur_id = current_invoice.efaktur_id
        efaktur_id = efaktur_id.action_efaktur_replace()
        new = current_invoice.copy()

        efaktur_id.update({
            'name': "%s%s-%s" % (
                current_invoice.efaktur_id.tin,
                efaktur_id.category,
                current_invoice.efaktur_id.number,
            ),
            'number': current_invoice.efaktur_id.number,
            'invoice_id': new.id,
        })

        new.update({
            'efaktur_id': efaktur_id.id,
            'replacement_id': current_invoice.id,
        })

        return {
            'name': _('Replacement of Invoice'),
            'view_mode': 'form',
            'res_model': 'account.invoice',
            'context': ctx,
            'view_id': view_id.id,
            'res_id': new.id,
            'target': 'current',
            'type': 'ir.actions.act_window',
        }
