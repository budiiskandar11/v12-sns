import base64
import csv
import io
# import os
# import stat
from datetime import datetime
from odoo import models, fields, api, _
from odoo.modules import get_module_path
from odoo.exceptions import ValidationError, UserError


class EFakturExportWizard(models.TransientModel):
    _name = "efaktur.export.wizard"
    _description = "E-Faktur Export Wizard"

    name = fields.Char('Reference/Description', readonly=True)
    efaktur_content_data = fields.Binary('Efaktur Content Data', readonly=True)
    filename = fields.Char(string='Filename', size=256, readonly=True)
    efaktur_ids = fields.Many2many(
        'efaktur', string='Selected Faktur Pajaks', readonly=True,
        default=lambda self: self._context.get('active_ids'),
    )
    is_exported = fields.Boolean(string='Has Exported', default=False, readonly=True)

    @api.multi
    def action_export_tax_invoice(self):
        filename, efaktur_content_data = 'reexport_efaktur.csv', ''
        # assigned_efaktur_ids = self.efaktur_ids.filtered(lambda efaktur: not efaktur.is_uploaded or not efaktur.is_verified and efaktur.state == 'assigned')
        assigned_efaktur_ids = self.efaktur_ids

        if not assigned_efaktur_ids:
            msg = "Only 'Nomor Faktur Pajak' that has received the Invoice number \
            and Not yet Submitted nor Verified!"
            raise ValidationError(_(msg))
        else:
            headers = [
                'FK',
                'KD_JENIS_TRANSAKSI',
                'FG_PENGGANTI',
                'NOMOR_FAKTUR',
                'MASA_PAJAK',
                'TAHUN_PAJAK',
                'TANGGAL_FAKTUR',
                'NPWP',
                'NAMA',
                'ALAMAT_LENGKAP',
                'JUMLAH_DPP',
                'JUMLAH_PPN',
                'JUMLAH_PPNBM',
                'ID_KETERANGAN_TAMBAHAN',
                'FG_UANG_MUKA',
                'UANG_MUKA_DPP',
                'UANG_MUKA_PPN',
                'UANG_MUKA_PPNBM',
                'REFERENSI'
            ]

            invoice_ids = assigned_efaktur_ids.mapped('invoice_id')
            csvfile = io.StringIO()
            csvwriter = csv.writer(csvfile, delimiter=',')
            csvwriter.writerow([h.upper() for h in headers])

            self.lt_sub_headers(headers, csvwriter)
            self.of_sub_headers(headers, csvwriter)

            for invoice_id in invoice_ids:
                self.fk_value(headers, csvwriter, invoice_id)
                self.fapr_value(headers, csvwriter,
                    self.env.user.company_id.partner_id)

                for line in invoice_id.invoice_line_ids:
                    self.of_value(headers, csvwriter, line)

            efaktur_content_data = csvfile.getvalue()
            csvfile.close()
            assigned_efaktur_ids.write({'state': 'submit', 'is_uploaded': True,})

        self.write({
            'efaktur_content_data': base64.encodestring(efaktur_content_data.encode()),
            'filename': filename,
            'is_exported': True,
            'name': 'Exported Today - %s' %(fields.Date.today()),
        })

        action = {
            'name': 'Download CSV',
            'type': 'ir.actions.act_url',
            'url': "web/content/?model=efaktur.export.wizard&id=" + str(self.id) + "&filename_field=filename&field=efaktur_content_data&download=true&filename=" + self.filename,
            'target': 'self',
        }
        return action

    def lt_sub_headers(self, headers, csvwriter):
        data = {
            'FK': 'LT',
            'KD_JENIS_TRANSAKSI': 'NPWP',
            'FG_PENGGANTI': 'NAMA',
            'NOMOR_FAKTUR': 'JALAN',
            'MASA_PAJAK': 'BLOK',
            'TAHUN_PAJAK': 'NOMOR',
            'TANGGAL_FAKTUR': 'RT',
            'NPWP': 'RW',
            'NAMA': 'KECAMATAN',
            'ALAMAT_LENGKAP': 'KELURAHAN',
            'JUMLAH_DPP': 'KABUPATEN',
            'JUMLAH_PPN': 'PROPINSI',
            'JUMLAH_PPNBM': 'KODE_POS',
            'ID_KETERANGAN_TAMBAHAN': 'NOMOR_TELEPON',
            'FG_UANG_MUKA': '',
            'UANG_MUKA_DPP': '',
            'UANG_MUKA_PPN': '',
            'UANG_MUKA_PPNBM': '',
            'REFERENSI': ''
        }
        csvwriter.writerow([data[v] for v in headers])

    def of_sub_headers(self, headers, csvwriter):
        data = {
            'FK': 'OF',
            'KD_JENIS_TRANSAKSI': 'KODE_OBJEK',
            'FG_PENGGANTI': 'NAMA',
            'NOMOR_FAKTUR': 'HARGA_SATUAN',
            'MASA_PAJAK': 'JUMLAH_BARANG',
            'TAHUN_PAJAK': 'HARGA_TOTAL',
            'TANGGAL_FAKTUR': 'DISKON',
            'NPWP': 'DPP',
            'NAMA': 'PPN',
            'ALAMAT_LENGKAP': 'TARIF_PPNBM',
            'JUMLAH_DPP': 'PPNBM',
            'JUMLAH_PPN': '',
            'JUMLAH_PPNBM': '',
            'ID_KETERANGAN_TAMBAHAN': '',
            'FG_UANG_MUKA': '',
            'UANG_MUKA_DPP': '',
            'UANG_MUKA_PPN': '',
            'UANG_MUKA_PPNBM': '',
            'REFERENSI': ''
        }
        csvwriter.writerow([data[v] for v in headers])

    def fk_value(self, headers, csvwriter, inv):
        if not inv.partner_id.npwp:
            raise UserError("Harap masukkan NPWP Customer %s" % inv.partner_id.name)
 
        if not inv.efaktur_id:
            raise UserError("Harap masukkan Nomor Seri Faktur Pajak Keluaran Invoice Nomor %s" % inv.number)

        # yyyy-mm-dd to dd/mm/yyyy
        d  = inv.date_invoice.split("-")
        date_invoice = "%s/%s/%s" %(d[2],d[1],d[0])
        npwp = inv.partner_id.npwp.replace(".", "").replace("-", "")
        faktur = inv.efaktur_id.number.replace(".", "").replace("-", "")

        date_published = inv.efaktur_id.date_published
        year_tax = datetime.strptime(str(date_published) +
                                         ' 00:00:00', '%Y-%m-%d 00:00:00').year
        month_tax = datetime.strptime(str(date_published) +
                                          ' 00:00:00', '%Y-%m-%d 00:00:00').month

        data = {
            'FK': 'FK',
            'KD_JENIS_TRANSAKSI': inv.efaktur_id.tin,
            'FG_PENGGANTI': inv.efaktur_id.category,
            'NOMOR_FAKTUR': faktur,
            'MASA_PAJAK': str(month_tax) or '',
            'TAHUN_PAJAK': str(year_tax) or '',
            'TANGGAL_FAKTUR': date_invoice,
            'NPWP': npwp,
            'NAMA': inv.partner_id.name or '',
            'ALAMAT_LENGKAP': inv.partner_id.street or '',
            'JUMLAH_DPP': int(round(inv.amount_untaxed, 0)) or 0,
            'JUMLAH_PPN': int(round(inv.amount_tax, 0)) or 0,
            'JUMLAH_PPNBM': 0,
            'ID_KETERANGAN_TAMBAHAN': '',
            'FG_UANG_MUKA': 0,
            'UANG_MUKA_DPP': 0,
            'UANG_MUKA_PPN': 0,
            'UANG_MUKA_PPNBM': 0,
            'REFERENSI': inv.number or ''
        }
        csvwriter.writerow([data[v] for v in headers])

    def fapr_value(self, headers, csvwriter, company):
        data = {
            'FK': 'FAPR',
            'KD_JENIS_TRANSAKSI': company.name,
            'FG_PENGGANTI': company.street,
            'NOMOR_FAKTUR': '',
            'MASA_PAJAK': '',
            'TAHUN_PAJAK': '',
            'TANGGAL_FAKTUR': '',
            'NPWP': '',
            'NAMA': '',
            'ALAMAT_LENGKAP': '',
            'JUMLAH_DPP': '',
            'JUMLAH_PPN': '',
            'JUMLAH_PPNBM': '',
            'ID_KETERANGAN_TAMBAHAN': '',
            'FG_UANG_MUKA': '',
            'UANG_MUKA_DPP': '',
            'UANG_MUKA_PPN': '',
            'UANG_MUKA_PPNBM': '',
            'REFERENSI': ''
        }
        csvwriter.writerow([data[v] for v in headers])

    def of_value(self, headers, csvwriter, line):
        harga_total = line.price_unit * line.quantity
        dpp = line.price_subtotal
        taxes = line.invoice_line_tax_ids.compute_all(
            dpp, line.invoice_id.currency_id,
            1, product=line.product_id,
            partner=line.invoice_id.partner_id)
        ppn = abs(harga_total - taxes['total_included'])

        data = {
            'FK': 'OF',
            'KD_JENIS_TRANSAKSI': line.product_id.default_code or '',
            'FG_PENGGANTI': line.product_id.display_name or '',
            'NOMOR_FAKTUR': line.price_unit,
            'MASA_PAJAK': line.quantity ,
            'TAHUN_PAJAK': harga_total,
            'TANGGAL_FAKTUR': line.discount or 0,
            'NPWP': dpp,
            'NAMA': ppn,
            'ALAMAT_LENGKAP': '0',
            'JUMLAH_DPP': '0',
            'JUMLAH_PPN': '',
            'JUMLAH_PPNBM': '',
            'ID_KETERANGAN_TAMBAHAN': '',
            'FG_UANG_MUKA': '',
            'UANG_MUKA_DPP': '',
            'UANG_MUKA_PPN': '',
            'UANG_MUKA_PPNBM': '',
            'REFERENSI': ''
        }
        csvwriter.writerow([data[v] for v in headers])

    name = fields.Char('Reference/Description', readonly=True)
    efaktur_ids = fields.Many2many(
        'efaktur', string='Selected Faktur Pajaks', readonly=True,
        default=lambda self: self._context.get('active_ids'),
    )
    is_exported = fields.Boolean(string='Has Exported', default=False, readonly=True)

############# old efaktur csv generate method #############
#     name = fields.Char('Reference/Description', readonly=True)
#     efaktur_ids = fields.Many2many(
#         'efaktur', string='Selected Faktur Pajaks', readonly=True,
#         default=lambda self: self._context.get('active_ids'),
#     )
#     is_exported = fields.Boolean(string='Has Exported', default=False, readonly=True)
# 
#     @api.multi
#     def action_export_tax_invoice(self):
#         """
#         This function will write data into freexport_efaktur.csv and
#         then, it will generate an e-faktur report.
#         """
#         filename = '/static/reexport_efaktur.csv'
#         not_assign_submit = self.efaktur_ids.filtered(
#             lambda efaktur: efaktur.state not in ['assign', 'submit'])
#         assign_submit = self.efaktur_ids.filtered(
#             lambda efaktur: efaktur.state in ['assign', 'submit'])
# 
#         if not_assign_submit:
#             msg = "Only 'Nomor Faktur Pajak' that has received the Invoice number \
#             and Not yet Exported!"
#             raise ValidationError(_(msg))
#         else:
#             mpath = get_module_path('efaktur')
#             # os.remove(str(mpath) + filename)
#             csvfile = open(mpath + filename, 'w+')
#             csvfile.write('FK,KD_JENIS_TRANSAKSI,FG_PENGGANTI,NOMOR_FAKTUR,MASA_PAJAK,TAHUN_PAJAK,TANGGAL_FAKTUR,NPWP,NAMA,ALAMAT_LENGKAP,JUMLAH_DPP,JUMLAH_PPN,JUMLAH_PPNBM,ID_KETERANGAN_TAMBAHAN,FG_UANG_MUKA,UANG_MUKA_DPP,UANG_MUKA_PPN,UANG_MUKA_PPNBM,REFERENSI\n')
#             csvfile.write(
#                 'LT,NPWP,NAMA,JALAN,BLOK,NOMOR,RT,RW,KECAMATAN,KELURAHAN,KABUPATEN,PROPINSI,KODE_POS,NOMOR_TELEPON,,,,,\n')
#             csvfile.write(
#                 'OF,NAMA,KODE_OBJEK,HARGA_SATUAN,JUMLAH_BARANG,HARGA_TOTAL,DISKON,DPP,PPN,TARIF_PPNBM,PPNBM,,,,,,,,\n')
# 
#             for faktur in assign_submit:
#                 year = datetime.strptime(str(faktur.date_published) +
#                                          ' 00:00:00', '%Y-%m-%d 00:00:00').year
#                 month = datetime.strptime(str(faktur.date_published) +
#                                           ' 00:00:00', '%Y-%m-%d 00:00:00').month
#                 date_published = datetime.strptime(faktur.date_published, "%Y-%m-%d").strftime(
#                     "%d/%m/%Y")
# 
#                 csvfile.write(
#                     'FK,' + str(faktur.partner_id.tin_manual) + ','
#                     + str(faktur.category) + ','
#                     + str(faktur.name) + ','
#                     + str(month) + ','
#                     + str(year) + ','
#                     + str(date_published) + ','
#                     + str(faktur.partner_id.npwp or '') + ','
#                     + str(faktur.partner_id.name or '') + ','
#                     + str(faktur.partner_id.city or '') + ' '
#                     + str(faktur.partner_id.country_id.name or '') + ','
#                     + str(int(faktur.invoice_id.amount_untaxed)) + ','
#                     + str(int(faktur.invoice_id.amount_tax)) + ','
#                     + '0,0,0,0,0,0,'
#                     + str(faktur.invoice_id.number) + '\n')
#                 csvfile.write('FAPR,' + str(self.env.user.company_id.name or '')
#                               + ',' + str(self.env.user.company_id.
#                                           street or '') + ',,,,,,,,,,,,,,,,\n')
# 
#                 for inv in faktur.invoice_id:
#                     for line in inv.invoice_line_ids:
#                         subtotal_price = line.quantity * line.price_unit
#                         price_disc = ((subtotal_price * line.discount) / 100)
#                         subtotal_without_disc = subtotal_price - price_disc
#                         total_tax = subtotal_without_disc
# 
#                         for tax in line.invoice_line_tax_ids:
#                             total_tax = total_tax - ((total_tax * tax.amount)) / 100
# 
#                         tax = subtotal_without_disc - total_tax
#                         dpp = total_tax
# 
#                         csvfile.write('OF,' + str(line.product_id.default_code)
#                                       + ',' + str(line.product_id.name)
#                                       + ',' + str(int(line.price_unit))
#                                       + ',' + str(int(line.quantity))
#                                       + ',' + str(int(subtotal_price))
#                                       + ',' + str(int(price_disc))
#                                       + ',' + str(int(dpp))
#                                       + ',' + str(int(tax))
#                                       + ',0,0,,,,,,,,\n')
# 
#             csvfile.close()
# 
#         self.write({
#             'is_exported': True,
#             'name': 'Exported Today - %s' % (fields.Date.today()),
#         })
# 
#         form_res = self.env.ref('efaktur.efaktur_export_view_form')
#         form_id = form_res and form_res.id or False
# 
#         self.change_state()
# 
#         return {
#             'name': _('Download XLS'),
#             'view_type': 'form',
#             'view_mode': 'form',
#             'res_model': 'efaktur.export.wizard',
#             'res_id': self.id,
#             'view_id': False,
#             'views': [(form_id, 'form')],
#             'type': 'ir.actions.act_window',
#             'target': 'current'
#         }
# 
#     def change_state(self):
#         assign_submit = self.efaktur_ids.filtered(
#             lambda efaktur: efaktur.state in ['assign', 'submit'])
#         for faktur in assign_submit:
#             if faktur.state == 'assign':
#                 self.efaktur_ids.write({'state': 'submit'})
