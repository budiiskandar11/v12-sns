from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError


class EFakturVerficationWizard(models.TransientModel):
    _name = "efaktur.verification.wizard"
    _description = "E-Faktur Verification Wizard"

    name = fields.Char('Reference/Description', readonly=True)
    efaktur_ids = fields.Many2many(
        'efaktur', string='Selected Faktur Pajaks', readonly=True,
        default=lambda self: self._context.get('active_ids'),
    )

    @api.multi
    def action_verification_tax_invoice(self):
        not_submit = self.efaktur_ids.filtered(lambda efaktur: efaktur.state != 'submit')

        if not_submit:
            msg = "Only 'Nomor Faktur Pajak' that has submitted into the DJP"
            raise ValidationError(_(msg))
        else:
            efaktur_type = self.efaktur_ids.mapped('type')
            self.efaktur_ids.write({'state': 'done'})
            self.write({
                'name': 'Verified Today - %s' %(fields.Date.today()),
            })

        self.ensure_one()
        view_id = self.env.ref('efaktur.efaktur_purchase_view_tree') \
            if efaktur_type == 'purchase' else \
                self.env.ref('efaktur.efaktur_sale_view_tree')

        return {
            'name': _(self.name),
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'efaktur',
            'view_id': view_id.id,
            'domain': [('id', 'in', self.efaktur_ids.ids)],
            'type': 'ir.actions.act_window',
        }
