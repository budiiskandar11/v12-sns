from odoo import models, fields, api, _


class EFakturGenerateNumberWizard(models.TransientModel):
    _name = "efaktur.generate.number.wizard"
    _description = "E-Faktur Generate Number Wizard"

    efaktur_type = fields.Selection([
        ('sale', 'Faktur Pajak Keluaran'),
        ('purchase', 'Faktur Pajak Masukkan'),
    ], string='Tipe Faktur Pajak', default='sale')
    date_published = fields.Date(
        string='Tanggal Terbit Pajak',
        default=fields.Date.today(),
        inverse='_inverse_year_tax_period'
    )
    ytn = fields.Char(
        string='Kode Identifikasi Tahun', size=2,
        default=lambda self: self.get_default_year_tax_number(),
        readonly=True,
        help="YTN is Year Tax Number. \n"
        "Only display two digits of Years based on Published Date (Tanggal Terbit Pajak)"
    )
    start_head_tsn = fields.Char(
        string='Kode Seri Faktur Pajak (Head/Short - 3 Digits) Awal',
        size=3, default='000',
        help="TSN is Tax Short Number. \n"
        "Has 3 Digits Head Short Series Number. \n"
        "Tax Full Number will be has 13 Digits. Breaks with Year Tax Series Number as 2 Digits"
    )
    start_tail_tsn = fields.Char(
        string='Kode Seri Faktur Pajak (Tail/Long - 8 Digits) Awal',
        size=8,
        help="TSN is Tax Short Number. \n"
        "Has 8 Digits Tail Long Series Number After (Head TSN + Year Tax Number). \n"
        "Tax Full Number will be has 13 Digits. Breaks with Year Tax Series Number as 2 Digits"
    )
    end_head_tsn = fields.Char(
        string='Kode Seri Faktur Pajak (Head/Short - 3 Digits) Akhir',
        size=3, default='000',
        help="TSN is Tax Short Number. \n"
        "Has 3 Digits Head Short Series Number. \n"
        "Tax Full Number will be has 13 Digits. Breaks with Year Tax Series Number as 2 Digits"
    )
    end_tail_tsn = fields.Char(
        string='Kode Seri Faktur Pajak (Tail/Long - 8 Digits) Akhir',
        size=8,
        help="TSN is Tax Short Number. \n"
        "Has 8 Digits Tail Long Series Number After (Tax Identification + Year Tax Number). \n"
        "Tax Full Number will be has 13 Digits. Breaks with Year Tax Series Number as 2 Digits"
    )

    @api.model
    def get_default_year_tax_number(self):
        date_published = fields.Date.today() if not self.date_published else self.date_published
        year = fields.Date.from_string(date_published).strftime('%y')
        return year

    def adjust_tax_series_number(self, current_tax_series_number):
        different_len_start_number = 11 - len(current_tax_series_number) or 0
        new_tax_series_number = '%s%s' %(
            current_tax_series_number, current_tax_series_number[:different_len_start_number])
        return new_tax_series_number

    @api.onchange('date_published')
    def year_tax_number_change(self):
        self.ytn = self.get_default_year_tax_number()

    @api.onchange('start_head_tsn', 'start_tail_tsn')
    def start_tax_series_number_change(self):
        if self.start_head_tsn and self.start_tail_tsn:
            tax_series_number = '%s%s' %(self.start_head_tsn, self.start_tail_tsn)
            if len(tax_series_number) < 11:
                current_start_number = tax_series_number
                new_start_number = self.adjust_tax_series_number(current_start_number)
                message = 'This number will be inputted as %s to %s' %(
                    current_start_number, new_start_number)
                warning_mess = {
                    'title': _('Below on 11 Digits of Nomor Faktur Awal!'),
                    'message' : message
                }
                return {'warning': warning_mess}

    @api.onchange('end_head_tsn', 'end_tail_tsn')
    def end_tax_series_number_change(self):
        if self.end_head_tsn and self.end_tail_tsn:
            tax_series_number = '%s%s' %(self.end_head_tsn, self.end_tail_tsn)
            if len(tax_series_number) < 11:
                current_end_number = tax_series_number
                new_end_number = self.adjust_tax_series_number(current_end_number)
                message = 'This number will be inputted as %s to %s' %(
                    current_end_number, new_end_number)
                warning_mess = {
                    'title': _('Below on 11 Digits of Nomor Faktur Akhir!'),
                    'message' : message
                }
                return {'warning': warning_mess}

    @api.onchange('start_head_tsn', 'start_tail_tsn', 'end_head_tsn', 'end_tail_tsn')
    def tax_series_number_comparison_size(self):
        if self.start_head_tsn and self.start_tail_tsn and self.end_head_tsn and self.end_tail_tsn:
            start_tax_series_number = '%s%s' %(self.start_head_tsn, self.start_tail_tsn)
            end_tax_series_number = '%s%s' %(self.end_head_tsn, self.end_tail_tsn)

            if start_tax_series_number > end_tax_series_number:
                message = 'Nomor Faktur Akhir should be greather than Nomor Faktur Awal'
                warning_mess = {
                    'title': _('Different Size between Nomor Faktur Awal dan Akhir!'),
                    'message' : message
                }
                return {'warning': warning_mess}

    @api.multi
    def _inverse_year_tax_period(self):
        self.ensure_one()
        self.year_tax_number_change()

    def _preparation_efaktur_vals(self, efaktur_full_number):
        vals = {
            'number': efaktur_full_number,
            'type': self.efaktur_type,
            'date_published': self.date_published,
        }
        return vals

    def _generate_efaktur_number(self):
        created_efaktur_ids = self.env['efaktur']

        start_tax_series_number = '%s%s' %(self.start_head_tsn, self.start_tail_tsn)
        end_tax_series_number = '%s%s' %(self.end_head_tsn, self.end_tail_tsn)

        current_start_series_number = start_tax_series_number
        current_end_series_number = end_tax_series_number

        start_series_number = int(self.adjust_tax_series_number(current_start_series_number))
        end_series_number = int(self.adjust_tax_series_number(current_end_series_number))

        while start_series_number <= end_series_number:
            index = len(current_start_series_number) - len(str(start_series_number))

            tax_series_number = '%s%s' %(
                current_start_series_number[:index], str(start_series_number))

            tax_head_series_number = tax_series_number[:3]
            tax_tail_series_number = tax_series_number[3:]

            efaktur_full_number = "%s.%s-%s" %(
                tax_head_series_number, self.ytn, tax_tail_series_number)

            data = self._preparation_efaktur_vals(efaktur_full_number)
            created_efaktur_ids |= created_efaktur_ids.create(data)
            start_series_number += 1

        return created_efaktur_ids

    @api.multi
    def action_generate_efaktur_number(self):
        self.ensure_one()
        view_id = self.env.ref('efaktur.efaktur_purchase_view_tree') \
            if self.efaktur_type == 'purchase' else \
                self.env.ref('efaktur.efaktur_sale_view_tree')

        created_efaktur_ids = self._generate_efaktur_number()
        return {
            'name': _('Created Today'),
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'efaktur',
            'view_id': view_id.id,
            'domain': [('id', 'in', created_efaktur_ids.ids), ('type', '=', self.efaktur_type)],
            'type': 'ir.actions.act_window',
        }
