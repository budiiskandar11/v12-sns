
from odoo.addons.account.tests.account_test_classes import AccountingTestCase
from odoo.modules import get_module_path
import time
import os


class TestEfaktur(AccountingTestCase):
    def setUp(self):
        super(TestEfaktur, self).setUp()
        self.invoice = self.env['account.invoice']
        self.partner = self.env['res.partner']
        self.coa = self.env['account.account']
        self.coa_type = self.env['account.account.type']
        self.journal = self.env['account.journal']

    def create_invoice(self, partner, amount, i_type, i_account, i_line_account):
        """Return Open Invoice"""
        invoice = self.invoice.create({
            'partner_id': partner.id,
            'name': 'test efaktur',
            'currency_id': self.env.ref('base.USD').id,
            'type': i_type,
            'account_id': i_account.id,
            'date': time.strftime('%Y') + '-01-15',
            'invoice_line_ids': [
                (0, 0, {
                    'name': 'Test',
                    'quantity': 1,
                    'price_unit': amount,
                    'account_id': i_line_account.id,
                }),
                (0, 0, {
                    'name': 'Test',
                    'quantity': 1,
                    'price_unit': amount,
                    'account_id': i_line_account.id,
                })
            ]
        })
        invoice.action_invoice_open()
        return invoice

    def test_efaktur(self):
        """Create Nomor Pajak Keluaran and Nomor Pajak Masukan
        Create Invoice and link it to Nomor pajak
        """
        coa_payable = self.coa.create({
            'code': '001',
            'name': 'Payable',
            'user_type_id': self.env.ref('account.data_account_type_payable').id,
            'reconcile': True
        })
        coa_receivable = self.coa.create({
            'code': '002',
            'name': 'Receivable',
            'user_type_id': self.env.ref('account.data_account_type_receivable').id,
            'reconcile': True,
        })
        coa_expense = self.coa.create({
            'code': '003',
            'name': 'Expense',
            'user_type_id': self.env.ref('account.data_account_type_expenses').id,
        })
        coa_income = self.coa.create({
            'code': '004',
            'name': 'Income',
            'user_type_id': self.env.ref('account.data_account_type_revenue').id,
        })

        customer = self.partner.create({
            'name': 'Customer',
            'property_account_payable_id': coa_payable.id,
            'property_account_receivable_id': coa_receivable.id,
            'supplier': False,
            'customer': True,
            'tin_manual': '03'
        })
        supplier = self.partner.create({
            'name': 'Customer',
            'property_account_payable_id': coa_payable.id,
            'property_account_receivable_id': coa_receivable.id,
            'supplier': True,
            'customer': False,
            'tin_manual': '02',
        })

        ci = self.create_invoice(customer, 5000, 'in_invoice',
                                 coa_receivable,
                                 coa_income)
        vb = self.create_invoice(supplier, 1700, 'out_invoice',
                                 coa_payable,
                                 coa_expense)

        # Create number of 'Faktur Pajak Keluaran'
        wiz_efaktur_keluar = self.env['efaktur.generate.number.wizard'].create({
            'efaktur_type': 'sale',
            'start_head_tsn': '000',
            'end_head_tsn': '000',
            'start_tail_tsn': '00000001',
            'end_tail_tsn': '00000002',
            'ytn': '18',
            'date_published': time.strftime('%Y') + '-01-15'
        })
        wiz_efaktur_keluar.action_generate_efaktur_number()
        efaktur_keluar = self.env['efaktur'].search([('name', 'like', '%00000001')])
        # Create number of 'Faktur Pajak Masukan'
        wiz_efaktur_masuk = self.env['efaktur.generate.number.wizard'].create({
            'efaktur_type': 'purchase',
            'start_head_tsn': '000',
            'end_head_tsn': '000',
            'start_tail_tsn': '00000005',
            'end_tail_tsn': '00000006',
            'ytn': '18',
            'date_published': time.strftime('%Y') + '-01-15'
        })
        wiz_efaktur_masuk.action_generate_efaktur_number()
        efaktur_masuk = self.env['efaktur'].search([('name', 'like', '%00000005')])
        self.assertTrue(efaktur_keluar.state == 'draft')
        self.assertTrue(efaktur_masuk.state == 'draft')

        ci.write({
            'efaktur_id': efaktur_keluar.id
        })
        vb.write({
            'efaktur_id': efaktur_masuk.id
        })
        self.assertTrue(efaktur_keluar.state == 'assign')
        self.assertTrue(efaktur_masuk.state == 'assign')

        # Download the templated csv file for user to upload to Goverment
        wiz_export = self.env['efaktur.export.wizard']. \
            with_context(active_ids=[efaktur_keluar.id, efaktur_masuk.id]). \
            create({})
        wiz_export.change_state()
        self.assertTrue(efaktur_keluar.state == 'submit')
        self.assertTrue(efaktur_masuk.state == 'submit')

        # Verification Faktur Pajak
        wiz_validate = self.env['efaktur.verification.wizard']. \
            with_context(active_ids=[efaktur_masuk.id, efaktur_keluar.id]). \
            create({})
        wiz_validate.action_verification_tax_invoice()
        self.assertTrue(efaktur_keluar.state == 'done')
        self.assertTrue(efaktur_masuk.state == 'done')
