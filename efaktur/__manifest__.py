
{
    'name': 'E-Faktur Management',
    'author': 'Portcities Ltd',
    'version': '11.0.1.0.0',
    'summary': 'Faktur Pajak Masukan dan Keluaran',
    'sequence': 1,
    'description': """
        This module will export file csv used to upload a tax
        invoice to E-Faktur for Indonesia Company
        """,
    'category': 'accounting',
    'website': "http://www.portcities.net",
    'depends': ['account_accountant'],
    'data': [
        'security/ir.model.access.csv',
        'wizards/efaktur_generate_number_wizard_view.xml',
        'wizards/efaktur_export_wizard_view.xml',
        'wizards/efaktur_verification_wizard_view.xml',
        'views/efaktur_view.xml',
        'views/efaktur_menuitem.xml',
        'views/account_invoice_view.xml',
        'views/res_partner_view.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False
}
