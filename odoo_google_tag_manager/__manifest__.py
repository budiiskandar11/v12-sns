# -*- coding: utf-8 -*-
##########################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
##########################################################################
{"name": "Google Tag Manager",
    "summary": "Deploy and update measurement tags on your websites without major code changes.",
    "category": "Website",
    "version": "1.0.0",
    "sequence": 1,
    "author": "Webkul Software Pvt. Ltd.",
    "license": "Other proprietary",
    "website": "https://store.webkul.com/Odoo-Google-Tag-Manager.html",
    "description": """
Use Google Tag Manager to manage tags (such as tracking and marketing optimization JavaScript tags) on your site. Without editing your site code, you use GTM user interface to add and update AdWords, Google Analytics, Floodlight, and non-Google tags. This reduces errors and allows you to to deploy tags on your site quickly.
    """,
    "live_test_url": "http://odoodemo.webkul.com/?module=odoo_google_tag_manager&version=12.0",
    "depends": ['website_sale'],
    "data": [
        # 'views/website_views.xml',
        'views/res_config_settings_views.xml',
        'views/snippets_template.xml',
    ],
    "images": ['static/description/Banner.png'],
    "application": True,
    "price": 45,
    "currency": "EUR",
    "pre_init_hook": "pre_init_check",
 }
