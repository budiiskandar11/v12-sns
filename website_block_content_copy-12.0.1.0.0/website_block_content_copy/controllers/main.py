# -*- coding: utf-8 -*-
##########################################################################
#
#   Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE file for full copyright and licensing details.
#   License URL : <https://store.webkul.com/license.html/>
#
##########################################################################


from odoo import http, _
from odoo.http import request


class CustomerPortal(http.Controller):

    @http.route(['/disable/configuration/'], type='json', website=True, auth="none")
    def get_disable_configuration(self):
        irDefaultSudo = request.env['ir.default'].sudo()
        allowMedia = []
        resModel = 'disable.block.content'
        disableRightclick = irDefaultSudo.get(resModel, 'disable_rightclick')
        disableF12 = irDefaultSudo.get(resModel, 'disable_f12')
        disableCopypaste = irDefaultSudo.get(resModel, 'disable_copypaste')
        disableCtrlS = irDefaultSudo.get(resModel, 'disable_ctrl_s')
        disableCtrlU = irDefaultSudo.get(resModel, 'disable_ctrl_u')
        disableCtrlShitI = irDefaultSudo.get(resModel, 'disable_ctrl_shit_i')
        disableCtrlShitC = irDefaultSudo.get(resModel, 'disable_ctrl_shit_c')
        res = {
            'disable_rightclick':disableRightclick,
            'disable_f12':disableF12,
            'disable_copypaste':disableCopypaste,
            'disable_ctrl_s':disableCtrlS,
            'disable_ctrl_u':disableCtrlU,
            'disable_ctrl_shit_i':disableCtrlShitI,
            'disable_ctrl_shit_c':disableCtrlShitC,
        }
        return res