# -*- coding: utf-8 -*-
##########################################################################
#
#	Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE file for full copyright and licensing details.
#   "License URL : <https://store.webkul.com/license.html/>"
#
##########################################################################

from odoo import api, models, fields


class DisableBlockContent(models.TransientModel):
    _name = 'disable.block.content'
    _inherit = 'res.config.settings'

    disable_rightclick = fields.Boolean(string='Disable Right Click')
    disable_f12 = fields.Boolean(string='Disable F12')
    disable_copypaste = fields.Boolean(string='Disable Ctrl+C, Ctrl+V, Ctrl+X')
    disable_ctrl_s = fields.Boolean(string='Disable Ctrl+S')
    disable_ctrl_u = fields.Boolean(string='Disable Ctrl+U')
    disable_ctrl_shit_i = fields.Boolean(string='Disable Ctrl+Shift+I')
    disable_ctrl_shit_c = fields.Boolean(string='Disable Ctrl+Shift+C')
    @api.multi
    def set_values(self):
        super(DisableBlockContent, self).set_values()
        irDefaultSudo = self.env['ir.default'].sudo()
        disableRightclick = self.disable_rightclick
        disableF12 = self.disable_f12
        disableCopypaste = self.disable_copypaste
        disableCtrlS = self.disable_ctrl_s
        disableCtrlU = self.disable_ctrl_u
        disableCtrlShitI = self.disable_ctrl_shit_i
        disableCtrlShitC = self.disable_ctrl_shit_c
        resModel = 'disable.block.content'
        irDefaultSudo.set(resModel, 'disable_rightclick', disableRightclick)
        irDefaultSudo.set(resModel, 'disable_f12', disableF12)
        irDefaultSudo.set(resModel, 'disable_copypaste', disableCopypaste)
        irDefaultSudo.set(resModel, 'disable_ctrl_s', disableCtrlS)
        irDefaultSudo.set(resModel, 'disable_ctrl_u', disableCtrlU)
        irDefaultSudo.set(resModel, 'disable_ctrl_shit_i', disableCtrlShitI)
        irDefaultSudo.set(resModel, 'disable_ctrl_shit_c', disableCtrlShitC)
        return True

    @api.model
    def get_values(self):
        res = super(DisableBlockContent, self).get_values()
        resModel = 'disable.block.content'
        irDefaultSudo = self.env['ir.default'].sudo()
        disableRightclick = irDefaultSudo.get(resModel, 'disable_rightclick')
        disableF12 = irDefaultSudo.get(resModel, 'disable_f12')
        disableCopypaste = irDefaultSudo.get(resModel, 'disable_copypaste')
        disableCtrlS = irDefaultSudo.get(resModel, 'disable_ctrl_s')
        disableCtrlU = irDefaultSudo.get(resModel, 'disable_ctrl_u')
        disableCtrlShitI = irDefaultSudo.get(resModel, 'disable_ctrl_shit_i')
        disableCtrlShitC = irDefaultSudo.get(resModel, 'disable_ctrl_shit_c')
        res.update({
            'disable_rightclick':disableRightclick,
            'disable_f12':disableF12,
            'disable_copypaste':disableCopypaste,
            'disable_ctrl_s':disableCtrlS,
            'disable_ctrl_u':disableCtrlU,
            'disable_ctrl_shit_i':disableCtrlShitI,
            'disable_ctrl_shit_c':disableCtrlShitC,
        })
        return res


    @api.model
    def _disable_block_content(self):
        configModel = self.env['disable.block.content'].sudo()
        disSetObj = configModel.create({
            'disable_rightclick':True,
            'disable_f12':True,
            'disable_copypaste':True,
            'disable_ctrl_s':True,
            'disable_ctrl_u':True,
            'disable_ctrl_shit_i':True,
            'disable_ctrl_shit_c':True,
        })
        disSetObj.execute()
        return True
