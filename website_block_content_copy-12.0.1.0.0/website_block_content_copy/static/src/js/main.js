odoo.define('website_block_content_copy.main', function (require) {
"use strict";

    require('web.dom_ready');
    var core = require('web.core');
    var ajax = require('web.ajax');
    
    $(document).ready(function() {
        ajax.jsonRpc("/disable/configuration/", 'call', {}).then(function (vals) {
            if (vals['disable_rightclick'] === true ) {
                $("body").on("contextmenu", function (e) { // Disable Right Click
                    return false;
                });
            }
            document.onkeydown = function (e) {
                if (vals['disable_f12'] === true && e.keyCode === 123) { // disable F12
                    return false;
                }

                if (vals['disable_copypaste'] === true) { // Disable Ctrl+C, Ctrl+V, Ctrl+X
                    $('body').bind('cut copy paste', function (e) {
                        e.preventDefault();
                    });
                }

                if (vals['disable_ctrl_s'] === true && e.ctrlKey && e.keyCode == 83) { // disable Ctrl+S
                    return false;
                }

                if (vals['disable_ctrl_u'] === true  && e.ctrlKey && (e.keyCode === 85)) { // disable Ctrl+U
                    return false;
                }

                if (vals['disable_ctrl_shit_i'] === true  && e.ctrlKey && e.shiftKey && e.keyCode == 73) { // disable Ctrl+Shift+I
                    return false;
                }

                if (vals['disable_ctrl_shit_c'] === true  && e.ctrlKey && e.shiftKey && e.keyCode == 67) { // disable Ctrl+Shift+C
                    return false;
                }

            };
        });



    })

});
