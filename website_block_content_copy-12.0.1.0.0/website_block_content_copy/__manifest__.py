# -*- coding: utf-8 -*-
##########################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
##########################################################################

{
  'name'                 :  'Website Stop/Block Content Copy',
  'version'              :  '1.0.0',
  'category'             :  'website',
  'author'               :  'Webkul Software Pvt. Ltd.',
  'website'              :  'https://store.webkul.com/Odoo-Website-Stop-Block-Content-Copy.html',
  'sequence'             :  1,
  'summary'              :  '''
Odoo Website Stop/Block Content Copy Module can help your website against 
content plagiarism and effectively stop the content from being copied from 
the website''',
  'description'          :  """Website Stop/Block Content Copy""",
  "live_test_url"        :  "http://odoodemo.webkul.com/?module=website_block_content_copy&version=12.0",
  'depends'              :  ['website', 'website_webkul_addons'],
  'data'                 :  [
                              'views/block_copy_content.xml',
                              'views/res_config_view.xml',
                            ],
  "images"        :  ['static/description/Banner.png'],
  'application'          :  True,
  'installable'          :  True,
  'active'               :  False,
  "price"                :  35,
  "currency"             :  "EUR",
  'pre_init_hook'        :  'pre_init_check',
}
