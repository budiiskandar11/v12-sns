# """
#  This module is developed by Portcities Indonesia
#  Copyright (C) 2017 Portcities Indonesia (<http://portcities.net>).
#  All Rights Reserved
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
# """

{
    "name": "SNS Custom Inventory",
    "summary": "Customize inventory",
    "version": "12.0.1.0.2",
    "author": "Portcities Ltd",
    "website" : "https://www.portcities.net",
    "license": "AGPL-3",
    "complexity": "normal",
    "description": """

v1.1
----
* Hide "Update Qty on Hand"
* Hide "Inventory Adjustment"

*Author : Thoharuddin Hanif.

v1.2
----
* Hide "Cost and Vendors in product for non purchasing user"
* Inventory Manager not granted Billing access"

*Author : Ugi.

                      """,
    "category": "Inventory",
    "depends": [
        'base',
        'stock',
        'purchase',
        'account',
        'point_of_sale',
    ],
    "data": [
        'security/portal_security.xml',
        'security/stock_security.xml',
        'views/custom_inventory_view.xml',
        'security/ir.model.access.csv',
    ],
    "test": [
    ],
    "auto_install": False,
    'installable': True,
    "application": False,
}
