""" model """
from odoo import models, fields
from os.path import expanduser
import base64
import os.path
import shutil
import logging

_logger = logging.getLogger(__name__)


class ProductProduct(models.Model):
    """ inherit product """
    _inherit = "product.product"

    def export_image_to_system(self):
        """
        function to export product image
        @return: image file as jpg
         """
        product_ids = self.env['product.template']
        user_dir = expanduser("~")
        home = "/tmp/SNS_image"
        _logger.info('Current user path directory is %s', user_dir)
        if not os.path.isdir(home):
            os.mkdir(home, 0o777)
            os.chmod(home, 0o777)
            _logger.info('Current path directory %s is created', home)
        for p_id in product_ids.search([]):
            binary_data = product_ids.search([('id', '=',p_id.id), ('type', '=', 'product')]).image
            if binary_data:
                data = base64.b64decode(binary_data)
                filez = home + "/" +  str(p_id.name) + ".jpg"
                with open(filez, "wb") as img_file:
                    img_file.write(data)

    def import_image_to_odoo(self):
        """
        function to import product image
        @return: image file
         """
        product_ids = self.env['product.template']
        user_dir = expanduser("~")
        home = "/tmp/SNS_image"
        _logger.info('Current user path directory is %s', user_dir)
        for p_id in product_ids.search([]):
            filez = home + "/" + str(p_id.name) + ".jpg"
            if os.path.isfile(filez):
                try:
                    with open(filez, "rb") as img_file:
                        image_data = base64.b64encode(img_file.read())
                    p_id.write({'image': image_data})
                    _logger.info('Importing image %s', filez)
                except ValueError:
                    _logger.info('File does not exist...')
        if os.path.isdir(home):
            shutil.rmtree(home, False, onerror=None)
