# -*- coding: utf-8 -*-
""" Partner localization """
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import models, fields, api

class ResCountrySubdistrict(models.Model):
    """ Define Sub District """

    _name = 'res.country.subdistrict.area'
    _description = 'Subdistrict Area'

    name = fields.Char('Subdistrict Area', required=True)
    subdistrict_id = fields.Many2one('res.country.subdistrict', 'Subdistrict')
    rajaongkir_id = fields.Integer('Rajaongkir Ref', index=True)


    _sql_constraints = [
        ('unique_rajaongkir_subdistrict',
         'unique(rajaongkir_id)',
         "Rajaongkir Ref Should be Unique!"),
    ]


class ResPartner(models.Model):
    """ Extend partner"""
    _inherit = 'res.partner'

    subdistrict_area_id = fields.Many2one('res.country.subdistrict.area', 'Subdistrict Area')
    subdistrict_area = fields.Char(related='subdistrict_area_id.name', store=True)

    @api.model
    def _address_fields(self):
        """Returns the list of address fields that are synced from the parent."""

        res = super(ResPartner, self)._address_fields()
        res += 'subdistrict_area',
        return res

    @api.onchange('subdistrict_area_id','subdistrict_id','city_id')
    def _onchange_subdistrict_area_id(self):
        if self.subdistrict_area_id:
            self.subdistrict_id = self.subdistrict_area_id.subdistrict_id
        if self.subdistrict_id :
            self.city_id = self.subdistrict_id.city_id
        if self.city_id :
            self.state_id = self.city_id.state_id

