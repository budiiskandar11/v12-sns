# """
#  This module is developed by Portcities Indonesia
#  Copyright (C) 2017 Portcities Indonesia (<http://portcities.net>).
#  All Rights Reserved
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
# """

{
    "name": "Locatization (add Kelurahan)",
    "summary": "Localization Indonesia",
    "version": "11.0.1.0.0",
    "author": "Budi Iskandar",
    "website" : "https://www.budiiskandar.net",
    "license": "AGPL-3",
    "complexity": "normal",
    "description": """
    Add Kelurahan on partner and related to shipping address and delivery charge
                      """,
    "category": "Sales",
    "depends": ['delivery_rajaongkir'],
    "data": [
            'views/partner_view.xml',
            # 'views/website_order.xml',
             ],
    "test": [],
    "auto_install": False,
    'installable': True,
    "application": False,
}
